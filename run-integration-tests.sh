#!/bin/bash

case "$1" in
    --help)
        echo "Integration tests runner"
        echo "Usage: ./run-integration-tests.sh [option]"
        echo "Options:"
        echo "  --help           display the help message"
        echo "  --auto-config    save the current configuration and use the default"
        echo "                   bitcoin configuration file \".bitcoin.conf\" for"
        echo "                   the tests"
        exit 0
        ;;
    --auto-config)
        if [ -f ~/.bitcoin/bitcoin.conf ]; then
            mv ~/.bitcoin/bitcoin.conf ~/.bitcoin/bitcoin.conf.bak
        fi
        cp .bitcoin.conf ~/.bitcoin/bitcoin.conf
        pkill bitcoind
        sleep 3
        bitcoind -daemon
        sleep 3
esac

if ! pgrep -x "bitcoind" > /dev/null; then
    echo "bitcoind is not running! starting \"bitcoind -daemon\"..."
    bitcoind -daemon
    sleep 3
fi

if [ ! -f ./.integration-tests ]; then
    # Generate the first 101 block in regtest mode
    echo "Generate 101 blocks..."
    bitcoin-cli -regtest generate 101
    touch ./.integration-tests
fi

# Print the amount available
echo "Available amount (BTC)"
bitcoin-cli getbalance

# Lauch integration tests
yarn integration:run

if [ "$1" == "--auto-config" ]; then
    if [ -f ~/.bitcoin/bitcoin.conf.bak ]; then
        mv ~/.bitcoin/bitcoin.conf.bak ~/.bitcoin/bitcoin.conf
        pkill bitcoind
        sleep 3
        bitcoind -daemon
        sleep 3
    fi
fi

