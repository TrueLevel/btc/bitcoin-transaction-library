Bitcoin One Way Channel Tx
======

Library purposes
-------------

This library is will be used by the "strategic library" to provide a complete implementation of payment channel over the bitcoin protocol. This library is not responsible for managing the state of the channel and handle network interaction, the other library do it.

This library has to provide two public interfaces: client and server. These interfaces provide helpers to:

- **create** all needed transactions for a given state
- **sign** a given transaction for a given state
- **verify** a given transaction for a given state


Installation
-------------

Install node.js and (optional) [yarn](https://yarnpkg.com/en/). Prefer yarn over npm, it's faster.

Install dependencies:

```bash
yarn install
```


Testing
-------------

### Execute linter

The linter config is locate in `tslint.json`. To execute the linter in console mode run:

```bash
yarn lint
```

### Execute tests

To compile, run the tests and the tests coverage:

```bash
yarn test
```

You can run tests in watch mode (the compileOnSave file in tsconfig is set to true):

```bash
yarn test:run -- --watch
```

### Execute integration tests

Integration tests use bitcoind in regtest mode (see [Regtest Mode on bitcoin.org](https://bitcoin.org/en/developer-examples#regtest-mode)).

The minimal bitcoin.conf file used for integration tests is:

```
##
## bitcoin.conf configuration file. Lines beginning with # are comments.
##

# Run a regression test network
regtest=1

#
# JSON-RPC options (for controlling a running Bitcoin/bitcoind process)
#

# server=1 tells Bitcoin-Qt and bitcoind to accept JSON-RPC commands
server=1

# If no rpcpassword is set, rpc cookie auth is sought. The default `-rpccookiefile` name
# is .cookie and found in the `-datadir` being used for bitcoind. This option is typically used
# when the server and client are run as the same user.
#
# If not, you must set rpcuser and rpcpassword to secure the JSON-RPC api. The first
# method(DEPRECATED) is to set this pair for the server and client:
rpcuser=test
rpcpassword=test

# Listen for RPC connections on this TCP port:
rpcport=18332
```

Two scripts are provided to run the integration tests with bitcoind with enought amount available and reset the regtest blockchain:

```bash
run-integration-tests.sh # start bitcoind if not running, mine 101 block if no blockchain, and run integration tests

reset-integration-tests.sh # errase flag file, errase regtest blockchain, and restart bitcoind to clear cache
```

### Write tests

Write tests in `test/` folder with the naming structure `[file/scenario].test.ts`. Group your tests in files, files is executed in parallel in separate threads and tests in each files is executed in parallel via async. Name carefully your tests. You can import all you want in the lib and dependencies.

```javascript
import test from 'ava';
import { crypto } from 'bitcoinjs-lib';

import { revHashFromSecret } from '../lib/common/secrets/revhash';
import { deriveRevSecret } from '../lib/common/secrets/revsecret';


test('A good name here', t => {
    t.deepEqual([1, 4, 3], [1, 4, 3]);
});
```

Building
-------------

### Building with tsc

Building with `tsc` compile all `.ts` present in `lib/` and `test/` and output the result in `build/`. The configuration is located in  `tsconfig.json`. To build with `tsc` run:

```bash
yarn build
```

### Building with webpack (create a bundle)

The actual webpack confing is in `config/webpack.config.debug.js`. The bundle is created in the `dist/` directory and contain a standalone bundle. To compile the `bundle.js` run:

```bash
yarn build:dist
```

The bundle is used by the demo application, be sure to compile it before running the demo.

### Building the documentation

The documentation is compiled with `typedoc` and the result is output into the `doc/` folder. To compile the documentation run:

```bash
yarn build:doc
```

**Serving the documentation**

```bash
cd doc/ && python -m SimpleHTTPServer 8000
```

Usage Example
-------------

Create a client for interacting with the channel and create some transactions:

```javascript
import channel from 'channel.js'

var channel = new channel.Channel(
  'tprv8ZgxMBicQKsPdy1GPK3UWbNVyvcttQqEBtQ3DprwSf5PRE7yuEzCijSxA5GtYNZdZP579QRA5nXSXXY9ZmXtQWNwma6UBQzkTBMChkvS7rm', // clientSeed
  'tpubDDmLggsXnurnvAZkyYomS43Qi9Ps9t62nN7nbCtPNnuN8a84HX5G2xBUr3MCmYyR4FYc73gukigAFrTKBYUPY41q5d88bETBHxcW5ChALoa', // providerAccPubkey
  4, // accountId
  'testnet' // network
)

channel.createSpendRefund(
  0, // i: number
  1, // n: number
  channel.Btc.fromSatoshis(200), // feePerByte: Btc
  'mkzsShsvbAZW2EgJnJoggzgkEQcyVk9L1k',
  2 // locktime
).then(res => {
  transaction.spendRefund = res
})
```

Create a server for interacting with the client and broadcast a hypothetic transaction:

```javascript
import channel from 'channel.js'

var serverChannel = new channel.ServerChannel(
  'tprv8ZgxMBicQKsPdKBmPRHBmU8MfbFoa96phv185cxBVTFewMfR6Z4oHqo3vNtYz2PwypxXU2Pvw5iKiz9RFhnpK6LFoUEu5zgMx6QDLF8YZbD', // providerSeed
  'tpubDCQCgbFWWaH2rxQrCYVUopEmBgnh7Q8BMESC1gC8dseRwLZv4UqhNwsMDRis1newBWXvZWKrmJuUu2KQsdHjzuCTRU4XadZjaJ5bUUPP99u', // clientAccPubkey
  4,
  'testnet' // network
)

serverChannel.getAdapter()
  .broadcastTx(transaction.settlementSignedByClientAndServer).then((res) => {
    transaction.settlementIsSpend = true
  })

```


Running demo
-------------

```bash
# go to demo folder
cd demo

# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn dev
```

Git conventions
-------------


### Branch naming convention

Start your branch name with one of these prefix.

```
feature/           Feature I'm adding or expanding
release/vX.X.X     Start a release
hotfix/            Hotfixes arise from the necessity to act immediately upon an undesired state of a live production version
```

Fee strategies (settlement & refund)
--------------

### Settlement Transaction

- Fees deducted from change amount (client pays all fees for settlement, but does not control when settlement happen) :s
- Fees deducted from settlement amount (provider pays all fees for settlement)
- Split fees.  This is possible but can get complicated as we would need to enforce minimum & maximum fee we consider valid.

### Refund Transaction

- Fees deducted from settlement amount (expensive + risk of attack?)
- Fees deducted from refund amount (client pays all fees)


To-Do
-----

- [X] Test the webpack bundle
- [X] Improve README
- [ ] More usage exemple
- [ ] Cosign
- [X] ChannelTx
- [X] refund + revoke refund

Typescript things

- [ ] Make key generation + bitcoinjs work in react native
