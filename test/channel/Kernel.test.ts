import { test } from 'ava';
import { HDNode, networks } from 'bitcoinjs-lib';
import { Kernel } from '../../lib/kernel/Kernel';
import { generateSeed } from '../../lib/common/key/genseed';

export class TestKernel extends Kernel {
  protected _accPriv: HDNode;
  protected _clientAccPub: HDNode;
  protected _serverAccPub: HDNode;
}

test('should have default argument for network, api and timelock', (t) => {
  t.plan(2);
  const kernel = new TestKernel(
    generateSeed(networks.testnet),
    HDNode.fromBase58(
      generateSeed(networks.testnet), networks.testnet
    ).neutered().toBase58(),
    4
  );
  t.is(kernel.getNetwork().wif, 0xef);
  t.is(kernel.getBlocksInFuture(), 10);
});
