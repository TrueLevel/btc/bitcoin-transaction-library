import test from 'ava';
import { getFeePerByte } from '../../../lib/common/api/feeapi';

test('can get fee estimate', async (t) => {
  try {
    const fee = await getFeePerByte('hourFee');
    t.is(typeof (fee.satoshis()), 'number');
  } catch (e) {
    t.fail(`should not throw this exception: ${e}`);
  }
});
