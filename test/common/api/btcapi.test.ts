import test from 'ava';
import { BTCApi } from '../../../lib/common/api/btcapi';

test('create a bitcoin rpc api', t => {
  try {
    BTCApi('bitcoin', 'btcrpc');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a regtest rpc api', t => {
  try {
    BTCApi('regtest', 'btcrpc');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a testnet rpc api', t => {
  try {
    BTCApi('testnet', 'btcrpc');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a bitcoin insight api', t => {
  try {
    BTCApi('bitcoin', 'insight');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a regtest insight api', t => {
  try {
    BTCApi('regtest', 'insight');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a testnet insight api', t => {
  try {
    BTCApi('testnet', 'insight');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});
