import test from 'ava';
import { BTCApiRPC } from '../../../lib/common/api/btcapirpc';

test('create a bitcoin BTCApiRPC object', t => {
  try {
    BTCApiRPC('bitcoin');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a testnet BTCApiRPC object', t => {
  try {
    BTCApiRPC('testnet');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a regtest BTCApiRPC object', t => {
  try {
    BTCApiRPC('regtest');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});
