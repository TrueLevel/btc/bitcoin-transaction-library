import test from 'ava';
import { Btc } from '../../../lib/common/types';
import { BTCApiInsight } from '../../../lib/common/api/btcapiinsight';

test('create a bitcoin BTCApiInsight object', t => {
  try {
    BTCApiInsight('bitcoin');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('create a testnet BTCApiInsight object', t => {
  try {
    BTCApiInsight('testnet');
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
  t.pass();
});

test('should return an error for regtest BTCApiInsight object', async t => {
  try {
    const api = BTCApiInsight('regtest');
    await api.getUtxos('mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc');
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exeception');
});

test('can get utxos from the testnet', async t => {
  try {
    const api = BTCApiInsight('testnet');
    const utxos = await api.getUtxos('mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc');
    const tot = utxos.reduce((acc, utxo) => acc.add(utxo.amount), Btc.zero());
    t.is(tot.satoshis(), 195000000);
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
});

test('can get utxos from the bitcoin network', async t => {
  try {
    const api = BTCApiInsight('bitcoin');
    const utxos = await api.getUtxos('19pta6x1hXzV9F5hHnhMARYbRjuxF6xbbV');
    const tot = utxos.reduce((acc, utxo) => acc.add(utxo.amount), Btc.zero());
    t.is(tot.satoshis(), 1222113);
    // https://blockchain.info/address/19pta6x1hXzV9F5hHnhMARYbRjuxF6xbbV
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
});

test('can estimate fee from the testnet', async t => {
  try {
    const api = BTCApiInsight('testnet');
    const estimatedFee = await api.getEstimateFee(6);
    t.is(estimatedFee.greaterThan(Btc.fromSatoshis(0)), true);
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
});

test('can estimate fee from the bitcoin network', async t => {
  try {
    const api = BTCApiInsight('bitcoin');
    const estimatedFee = await api.getEstimateFee(6);
    t.is(estimatedFee.greaterThan(Btc.fromSatoshis(0)), true);
  } catch (e) {
    t.fail('should not throw an exeception');
    return;
  }
});
