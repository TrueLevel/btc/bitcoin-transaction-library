import { test } from 'ava';
import { Btc } from '../../lib/common/btc';

test('cannot divide a satoshi in 0.5 satoshi', (t) => {
  const s1 = Btc.fromSatoshis(1);
  const s2 = Btc.fromSatoshis(2);
  t.is(s1.div(s2).satoshis(), 0);
});

test('get the right amout of satoshis from bitcoin', t => {
  try {
    const btc = Btc.fromBitcoin(1.3);
    t.is(1.3 * 10e7, btc.satoshis());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('should throw an exception with too large bitcoin number', t => {
  try {
    Btc.fromBitcoin(22000000);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('should throw an exception with too small bitcoin number', t => {
  try {
    Btc.fromBitcoin(0.000000009);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('should throw an exception with negative bitcoin number', t => {
  try {
    Btc.fromBitcoin(-1);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('should throw an exception with negative satoshis number', t => {
  try {
    Btc.fromBitcoin(-1);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('get one satoshi from bitcoin', t => {
  try {
    const btc = Btc.fromBitcoin(0.00000001);
    t.is(1, btc.satoshis());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('get 0.00000001 bitcoin from one satoshi', t => {
  try {
    const btc = Btc.fromSatoshis(1);
    t.is(0.00000001, btc.bitcoin());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('should throw an exception with floating number of satoshis', t => {
  try {
    Btc.fromSatoshis(1.000000000000001);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('should throw an exception with big floating number of bitcoin', t => {
  try {
    Btc.fromBitcoin(0.000000009);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('get the right amount of bitcoin from satoshis', t => {
  try {
    const btc = Btc.fromSatoshis(10e7);
    t.is(1, btc.bitcoin());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('can add to Btc objects', t => {
  t.plan(2);
  try {
    const btc1 = Btc.fromBitcoin(1);
    const btc2 = Btc.fromBitcoin(1);
    t.is(2, btc1.add(btc2).bitcoin());
    t.is(2 * 10e7, btc1.add(btc2).satoshis());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('can substract to Btc objects', t => {
  t.plan(2);
  try {
    const btc1 = Btc.fromBitcoin(1);
    const btc2 = Btc.fromBitcoin(0.005);
    t.is(0.995, btc1.sub(btc2).bitcoin());
    t.is(99500000, btc1.sub(btc2).satoshis());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});


test('high precision substraction of two Btc objects', t => {
  t.plan(1);
  try {
    const btc1 = Btc.fromSatoshis(2 + 1e14);
    const btc2 = Btc.fromSatoshis(1 + 1e14);
    t.is(1, btc1.sub(btc2).satoshis());
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('test equality of two Btc objects', t => {
  t.plan(3);
  try {
    const btc1 = Btc.fromBitcoin(1);
    const btc2 = Btc.fromSatoshis(10e7);
    const btc3 = Btc.fromBitcoin(0.9);
    t.is(true, btc1.equals(btc2));
    t.is(false, btc1.equals(btc3));
    t.is(false, btc2.equals(btc3));
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }
});

test('test greater / less than of two Btc objects', t => {
  t.plan(7);
  try {
    const btc1 = Btc.fromBitcoin(1);
    const btc2 = Btc.fromBitcoin(0.9);
    t.is(true, btc1.greaterThan(btc2));
    t.is(false, btc1.lessThan(btc2));
    t.is(true, btc1.greaterThanOrEqual(btc1));
    t.is(true, btc1.greaterThanOrEqual(btc2));
    t.is(false, btc2.greaterThanOrEqual(btc1));
    t.is(true, btc1.lessThanOrEqual(btc1));
    t.is(false, btc1.lessThanOrEqual(btc2));
  } catch (e) {
    t.fail('should not throw an exception');
    return;
  }

});
