import { test } from 'ava';
import { stateTransition } from '../../../lib/common/transaction/helper';
import { Btc } from '../../../lib/common/types';


test('should throw when creating invalid state', (t) => {
  const clientBalance1: Btc = Btc.fromSatoshis(10001);
  const clientBalance2: Btc = Btc.fromSatoshis(10000);
  const providerBalance: Btc = Btc.fromSatoshis(10000);
  const channelBalance: Btc = Btc.fromSatoshis(20005);
  try {
    const fee: Btc = Btc.fromSatoshis(5);
    const s = stateTransition(
      { noBroadcast: clientBalance1, afterBroadcast: clientBalance2 },
      { noBroadcast: providerBalance, afterBroadcast: providerBalance },
      { noBroadcast: channelBalance, afterBroadcast: channelBalance },
      { noBroadcast: fee, afterBroadcast: Btc.zero() },
      { noBroadcast: [0, 0], afterBroadcast: [1, 0] },
    );
    t.fail(`${s}`);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exception');
});

test('should create a new object', (t) => {
  const clientBalance2: Btc = Btc.fromSatoshis(10000);
  const providerBalance: Btc = Btc.fromSatoshis(10000);
  const channelBalance: Btc = Btc.fromSatoshis(20005);
  const fee: Btc = Btc.fromSatoshis(5);
  const s = stateTransition(
    { noBroadcast: clientBalance2, afterBroadcast: clientBalance2 },
    { noBroadcast: providerBalance, afterBroadcast: providerBalance },
    { noBroadcast: channelBalance, afterBroadcast: channelBalance },
    { noBroadcast: fee, afterBroadcast: fee },
    { noBroadcast: [1, 0], afterBroadcast: [0, 1] }
  );
  t.is(typeof s, 'object');
});
