import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import withdrawTx from '../../../lib/common/transaction/withdrawtx';
import { Btc, Utxo } from '../../../lib/common/types';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
    'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eAnDY8r\
BcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
    'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1XiWhg\
9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    settlementAddress: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
    withdrawAddress: 'mkzsShsvbAZW2EgJnJoggzgkEQcyVk9L1k',
    changeAddress: 'mryYksgUFKL7rCuwtFsDdLXBHifzMQN6Ft',
    utxos: <Utxo[]> [
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
        '5feb9ab57e231c69ba79088a4b5c43882f2d4ff668ce973cbb6ce86f210ceb5b',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(1.3)
      }
    ],
    account: 2,
    n: 1
  };
});

test('has the correct number of inputs and outputs', t => {
  t.plan(2);

  const res = withdrawTx(
    t.context.data.utxos,
    t.context.data.settlementAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.withdrawAddress,
    Btc.fromBitcoin(0.4),
    t.context.data.changeAddress,
    Btc.fromBitcoin(0.6),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(1, tx.ins.length);
  t.is(3, tx.outs.length);
});

test('throw an exception if the total amount is not correct', t => {
  try {
    withdrawTx(
      t.context.data.utxos,
      t.context.data.settlementAddress,
      Btc.fromBitcoin(0.4),
      t.context.data.withdrawAddress,
      Btc.fromBitcoin(0.4),
      t.context.data.changeAddress,
      Btc.fromBitcoin(0.6),
      Btc.fromSatoshis(2000),
      bitcoin.networks.testnet
    );
  } catch (e) {
    // WRONG TOTAL 1.4 != 1.3
    t.pass();
    return;
  }
  t.fail('should throw an exeception');
});

test('throw an exception if the amount is less than zero', t => {
  try {
    withdrawTx(
      t.context.data.utxos,
      t.context.data.settlementAddress,
      Btc.fromBitcoin(0.3),
      t.context.data.withdrawAddress,
      Btc.fromBitcoin(1),
      t.context.data.changeAddress,
      Btc.fromBitcoin(0),
      Btc.fromSatoshis(2000),
      bitcoin.networks.testnet
    );
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should throw an exeception');
});

test("as the correct transaction's outputs amount", t => {
  t.plan(3);
  const res = withdrawTx(
    t.context.data.utxos,
    t.context.data.settlementAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.withdrawAddress,
    Btc.fromBitcoin(0.4),
    t.context.data.changeAddress,
    Btc.fromBitcoin(0.6),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(30000000, tx.outs[0].value);
  t.is(40000000, tx.outs[1].value);
  t.is(59340000, tx.outs[2].value);
});

test('create a non-signed transaction', t => {
  const res = withdrawTx(
    t.context.data.utxos,
    t.context.data.settlementAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.withdrawAddress,
    Btc.fromBitcoin(0.4),
    t.context.data.changeAddress,
    Btc.fromBitcoin(0.6),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );

  t.plan(2);
  t.is(res.builder.inputs.length, 1);
  t.is(Object.keys(res.builder.inputs[0]).length, 0);
});
