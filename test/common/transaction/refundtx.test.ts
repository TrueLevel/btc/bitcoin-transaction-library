import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import { refundTransaction } from '../../../lib/common/transaction/refundtx';
import { Btc, Utxo } from '../../../lib/common/types';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
    'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eAnDY8r\
BcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
    'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1XiWhg\
9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    providerAddress: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
    utxos: <Utxo[]> [
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
        '5feb9ab57e231c69ba79088a4b5c43882f2d4ff668ce973cbb6ce86f210ceb5b',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(1.3)
      }
    ],
    revPubKey: {
      address: '2N1QsvK1j3tDtDAmwjzUnakNTqQ5pD9VyHK',
      redeemscript: Buffer.from(
        '632102bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa9\
ac51b275672103d0b18bbd4c06ee9da663f901546fda4ccb1616f09bf0b364305a34044ff66500a\
da91429a3e606209532822d35c5b8686721dbde2f675a8768'
      ),
      clientrefundnode: bitcoin.HDNode.fromBase58(
        'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eAnDY\
8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
        bitcoin.networks.testnet
      ),
      providerrefundnode: bitcoin.HDNode.fromBase58(
        'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1XiW\
hg9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
        bitcoin.networks.testnet
      )
    },
    account: 2,
    n: 0,
    i: 0
  };
});

test('create a valid first refund', t => {
  t.plan(2);

  const res = refundTransaction(
    t.context.data.utxos,
    t.context.data.revPubKey.address,
    Btc.zero(),
    Btc.fromBitcoin(1.3),
    Btc.fromSatoshis(20000),
    t.context.data.providerAddress,
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(1, tx.ins.length);
  t.is(1, tx.outs.length);
});

test('create a valid refund', t => {
  t.plan(2);

  const res = refundTransaction(
    t.context.data.utxos,
    t.context.data.revPubKey.address,
    Btc.fromBitcoin(1),
    Btc.fromBitcoin(0.3),
    Btc.fromSatoshis(20000),
    t.context.data.providerAddress,
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(1, tx.ins.length);
  t.is(2, tx.outs.length);
});

test('use the given revPubKey address', t => {
  const res = refundTransaction(
    t.context.data.utxos,
    t.context.data.revPubKey.address,
    Btc.fromBitcoin(1),
    Btc.fromBitcoin(0.3),
    Btc.fromSatoshis(20000),
    t.context.data.providerAddress,
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);
  const address = bitcoin.address.fromOutputScript(
    tx.outs[0].script,
    bitcoin.networks.testnet
  );

  t.is(t.context.data.revPubKey.address, address);
});

test('create a non-signed transaction', t => {
  const res = refundTransaction(
    t.context.data.utxos,
    t.context.data.revPubKey.address,
    Btc.fromBitcoin(1),
    Btc.fromBitcoin(0.3),
    Btc.fromSatoshis(20000),
    t.context.data.providerAddress,
    bitcoin.networks.testnet
  );

  t.plan(2);
  t.is(res.builder.inputs.length, 1);
  t.is(Object.keys(res.builder.inputs[0]).length, 0);
});
