import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import settlementTx from '../../../lib/common/transaction/settlementtx';
import { Btc, Utxo } from '../../../lib/common/types';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
      'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eAnDY8r\
BcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
      'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1XiWhg\
9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    providerAddress: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
    utxos: <Utxo[]> [
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
          '5feb9ab57e231c69ba79088a4b5c43882f2d4ff668ce973cbb6ce86f210ceb5b',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(1.3)
      }
    ],
    nextMultiSigAddress: '2N1QsvK1j3tDtDAmwjzUnakNTqQ5pD9VyHK',
    account: 2,
    n: 0
  };
});

test('has the correct number of inputs and outputs', t => {
  t.plan(2);

  const res = settlementTx(
    t.context.data.utxos,
    t.context.data.providerAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.nextMultiSigAddress,
    Btc.fromBitcoin(1),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(1, tx.ins.length);
  t.is(2, tx.outs.length);
});

test('has the correct amount of satoshis', t => {
  t.plan(2);

  const res = settlementTx(
    t.context.data.utxos,
    t.context.data.providerAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.nextMultiSigAddress,
    Btc.fromBitcoin(1),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(29400000, tx.outs[0].value);
  t.is(10e7, tx.outs[1].value);
});

test('has the correct transaction outputs', t => {
  t.plan(2);

  const res = settlementTx(
    t.context.data.utxos,
    t.context.data.providerAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.nextMultiSigAddress,
    Btc.fromBitcoin(1),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(
    '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
    tx.outs[0].script.toString('hex')
  );
  t.is(
    'a914599469deaded5c49a9ba93c777d892f6c38fccf487',
    tx.outs[1].script.toString('hex')
  );
});

test('create a non-signed transaction', t => {
  const res = settlementTx(
    t.context.data.utxos,
    t.context.data.providerAddress,
    Btc.fromBitcoin(0.3),
    t.context.data.nextMultiSigAddress,
    Btc.fromBitcoin(1),
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );

  t.plan(2);
  t.is(res.builder.inputs.length, 1);
  t.is(Object.keys(res.builder.inputs[0]).length, 0);
});
