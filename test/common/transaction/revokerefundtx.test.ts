import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import revokeRefundTx from '../../../lib/common/transaction/revokerefundtx';
import { Btc, Utxo } from '../../../lib/common/types';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
    'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eA\
nDY8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
    'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1\
XiWhg9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    providerAddress: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
    revSecret: 'cQYvrQcEwAGFwsMUmn7FT4XEVwAUfAiPrNqSbjmC5HQrwnz6Zp2x',
    utxos: <Utxo[]> [
      {
        address: '2N1QsvK1j3tDtDAmwjzUnakNTqQ5pD9VyHK',
        txid:
        '6d91346c985a45828bf477554ddb9ee10ef01fed7a96fd50f62b281516002909',
        vout: 0,
        scriptPubKey: 'a914599469deaded5c49a9ba93c777d892f6c38fccf487',
        amount: Btc.fromBitcoin(0.24)
      }
    ],
    revPubKey: {
      address: '2N1QsvK1j3tDtDAmwjzUnakNTqQ5pD9VyHK',
      redeemscript: Buffer.from(
        '632102bad37480864ce0d640ea6ad84999093e668e7319\
3d6d2fcc5f4aa961d13d0aa9ac51b275672103d0b18bbd4c06ee9da663f901546fda4ccb1616f09\
bf0b364305a34044ff66500ada91429a3e606209532822d35c5b8686721dbde2f675a8768'
      ),
      clientrefundnode: bitcoin.HDNode.fromBase58(
        'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eA\
nDY8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
        bitcoin.networks.testnet
      ),
      providerrefundnode: bitcoin.HDNode.fromBase58(
        'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1\
XiWhg9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
        bitcoin.networks.testnet
      )
    },
    account: 2,
    n: 0,
    i: 0
  };
});

test('has well formed output raw transaction', t => {
  t.plan(2);
  const res = revokeRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.providerAddress,
    t.context.data.revPubKey.redeemscript,
    t.context.data.revSecret,
    Btc.fromSatoshis(20000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(1, tx.ins.length);
  t.is(1, tx.outs.length);
});

test('has the correct transaction inputs', t => {
  const res = revokeRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.providerAddress,
    t.context.data.revPubKey.redeemscript,
    t.context.data.revSecret,
    Btc.fromSatoshis(20000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);
  t.is(
    '0047304402204603792ac9ce92c9cd2734a13700ea538985fc46737accab69eb42baf2a4ae\
bd02203329a43648eb99436e38dab08ab3f51ea08ab5dd44ec08950af59e6df5d4b7b301004cc63\
6333231303262616433373438303836346365306436343065613661643834393939303933653636\
3865373331393364366432666363356634616139363164313364306161396163353162323735363\
7323130336430623138626264346330366565396461363633663930313534366664613463636231\
3631366630396266306233363433303561333430343466663636353030616461393134323961336\
5363036323039353332383232643335633562383638363732316462646532663637356138373638'
    , tx.ins[0].script.toString('hex')
  );
});

test('has the correct transaction outputs', t => {
  const outScript = bitcoin.address.toOutputScript(
    t.context.data.providerAddress,
    bitcoin.networks.testnet
  );
  const res = revokeRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.providerAddress,
    t.context.data.revPubKey.redeemscript,
    t.context.data.revSecret,
    Btc.fromSatoshis(20000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.deepEqual(tx.outs[0].script, outScript);
});

test('has the right signature script for revPubKey', t => {
  const res = revokeRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.providerAddress,
    t.context.data.revPubKey.redeemscript,
    t.context.data.revSecret,
    Btc.fromSatoshis(20000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  const scriptSig = tx.ins[0].script;
  t.is('OP_0 304402204603792ac9ce92c9cd2734a13700ea538985fc46737accab69eb42baf2\
a4aebd02203329a43648eb99436e38dab08ab3f51ea08ab5dd44ec08950af59e6df5d4b7b301\
 OP_0 36333231303262616433373438303836346365306436343065613661643834393939303\
9336536363865373331393364366432666363356634616139363164313364306161396163353\
1623237353637323130336430623138626264346330366565396461363633663930313534366\
6646134636362313631366630396266306233363433303561333430343466663636353030616\
4613931343239613365363036323039353332383232643335633562383638363732316462646\
532663637356138373638',
    bitcoin.script.toASM(scriptSig)
  );
});
