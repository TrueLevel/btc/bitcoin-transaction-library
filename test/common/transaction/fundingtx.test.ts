import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import { fundingTransaction } from '../../../lib/common/transaction/fundingtx';
import { Btc, Utxo } from '../../../lib/common/types';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
    'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eA\
nDY8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    fundingAddress: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
    utxos: <Utxo[]> [
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
        '5feb9ab57e231c69ba79088a4b5c43882f2d4ff668ce973cbb6ce86f210ceb5b',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(1.3)
      },
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
        'e6f6e6d6dc4185804229b841faeaa5b769737c41cc142b3eed59f4c751369e2c',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(0.65)
      }
    ],
    account: 2,
    n: 0,
    i: 0
  };
});

test('has the right amount of input/output for one utxo', t => {
  t.plan(2);

  const fundingNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const res = fundingTransaction(
    [t.context.data.utxos[0]].map((utxo) =>
      ({ utxo, wif: fundingNode.keyPair.toWIF() })),
    'miqFaRxucGK7nwMkVRSAZbLCsbAazczFsX',
    Btc.fromSatoshis(10000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);
  t.is(1, tx.ins.length);
  t.is(1, tx.outs.length);
});

test('create the right transaction with one utxo', t => {
  const fundingNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const res = fundingTransaction(
    [t.context.data.utxos[0]].map((utxo) =>
      ({ utxo, wif: fundingNode.keyPair.toWIF() })),
    'miqFaRxucGK7nwMkVRSAZbLCsbAazczFsX',
    Btc.fromSatoshis(10000),
    bitcoin.networks.testnet
  );

  t.is(
    '01000000015beb0c216fe86cbb3c97ce68f64f2d2f88435c4b8a0879ba691c237eb59ae\
b5f000000006a47304402206ba0926efe8c678be569c142cd3f3a1fb642e71594d2d2443b618c05\
7f7cd7fe02207909f4e6c34b78eed7107c9cb4dd7fff5c6b5a71a7204538fe06d4489705b53f012\
1027e194f45f82228a9fd0365b0714f58f8506c45c8010f760980e300fa4e066fb2ffffffff0180\
769d07000000001976a914245e4a8fbea39628ab41ceca1f18bdc2d2b00c2d88ac00000000',
    res.tx
  );
});

test('create the right transaction with multiple utxos', t => {
  const fundingNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const res = fundingTransaction(
    t.context.data.utxos.map((utxo) =>
      ({ utxo, wif: fundingNode.keyPair.toWIF() })),
    'miqFaRxucGK7nwMkVRSAZbLCsbAazczFsX',
    Btc.fromSatoshis(10000),
    bitcoin.networks.testnet
  );

  t.is(
    '01000000025beb0c216fe86cbb3c97ce68f64f2d2f88435c4b8a0879ba691c237eb59ae\
b5f000000006a47304402205b79f65c7bc1b18a9faba92f6b9e721c070fde4b4c3d7d8a33c6a8bc\
9c264e0302201461ea435ad572fdb4ce7538b299ceb0ff3f076d9d8262f5bd43023d497d5e79012\
1027e194f45f82228a9fd0365b0714f58f8506c45c8010f760980e300fa4e066fb2ffffffff2c9e\
3651c7f459ed3e2b14cc417c7369b7a5eafa41b82942808541dcd6e6f6e6000000006a473044022\
02e8811d58e831b8064543bd40921d7c0aceeda11966fc2be765d8e18c854945302204350c4dc74\
3b74e437a3560314644f4eec37b4c1ce300497689764958e26f0720121027e194f45f82228a9fd0\
365b0714f58f8506c45c8010f760980e300fa4e066fb2ffffffff0180d1610b000000001976a914\
245e4a8fbea39628ab41ceca1f18bdc2d2b00c2d88ac00000000',
    res.tx
  );
});

test('has the right amount of input/output for multiple utxos', t => {
  t.plan(2);

  const fundingNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const res = fundingTransaction(
    t.context.data.utxos.map((utxo) =>
      ({ utxo, wif: fundingNode.keyPair.toWIF() })),
    'miqFaRxucGK7nwMkVRSAZbLCsbAazczFsX',
    Btc.fromSatoshis(10000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(2, tx.ins.length);
  t.is(1, tx.outs.length);
});

test('estimate the correct transaction fee', t => {
  const fee = Btc.fromSatoshis(1000);
  const totalBytes = t.context.data.utxos.length * 180 + 34 + 10;
  const total = fee.mul(totalBytes);

  const fundingNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const res = fundingTransaction(
    t.context.data.utxos.map((utxo) =>
      ({ utxo, wif: fundingNode.keyPair.toWIF() })),
    'miqFaRxucGK7nwMkVRSAZbLCsbAazczFsX',
    fee,
    bitcoin.networks.testnet
  );

  t.is(total.satoshis(), res.channelState.fee.afterBroadcast.satoshis());
});

test('correctly sign the transaction', t => {
  const fundingNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const res = fundingTransaction(
    t.context.data.utxos.map((utxo) =>
      ({ utxo, wif: fundingNode.keyPair.toWIF() })),
    'miqFaRxucGK7nwMkVRSAZbLCsbAazczFsX',
    Btc.fromSatoshis(1000),
    bitcoin.networks.testnet
  );

  const signature = bitcoin.ECSignature.parseScriptSignature(
    res.builder.inputs[0].signatures[0]
  );

  const sig = fundingNode.keyPair.verify(
    res.builder.tx.hashForSignature(
      0,
      res.builder.inputs[0].prevOutScript,
      bitcoin.Transaction.SIGHASH_ALL
    ),
    signature.signature
  );

  t.is(true, sig);
});
