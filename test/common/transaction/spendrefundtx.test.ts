import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import spendRefundTx from '../../../lib/common/transaction/spendrefundtx';
import { Btc, Utxo } from '../../../lib/common/types';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
      'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eAnDY8r\
BcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
      'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1XiWhg\
9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    recipientAddress: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
    utxos: <Utxo[]> [
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
          '5feb9ab57e231c69ba79088a4b5c43882f2d4ff668ce973cbb6ce86f210ceb5b',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(1.3)
      },
      {
        address: 'mgTc1MS5b3MRexE4iM6YgaZU8TXhxmCrvc',
        txid:
          'e6f6e6d6dc4185804229b841faeaa5b769737c41cc142b3eed59f4c751369e2c',
        vout: 0,
        scriptPubKey: '76a9140a55f13f5e75c764488d1fbab1240f9be5e41fc688ac',
        amount: Btc.fromBitcoin(0.65)
      }
    ],
    revPubKey: {
      address: '2N1QsvK1j3tDtDAmwjzUnakNTqQ5pD9VyHK',
      redeemscript: Buffer.from(
        '632102bad37480864ce0d640ea6ad84999093e668e7319\
3d6d2fcc5f4aa961d13d0aa9ac51b275672103d0b18bbd4c06ee9da663f901546fda4ccb1616f09\
bf0b364305a34044ff66500ada91429a3e606209532822d35c5b8686721dbde2f675a8768'
      ),
      clientrefundnode: bitcoin.HDNode.fromBase58(
        'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eA\
nDY8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
        bitcoin.networks.testnet
      ),
      providerrefundnode: bitcoin.HDNode.fromBase58(
        'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1\
XiWhg9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
        bitcoin.networks.testnet
      )
    },
    account: 2,
    n: 1
  };
});

test('has the correct number of inputs and outputs', t => {
  t.plan(2);
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(2, tx.ins.length);
  t.is(1, tx.outs.length);
});

test('has the correct sequence number', t => {
  const TESTED_SEQUENCE_NUM = 6;
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    TESTED_SEQUENCE_NUM,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(TESTED_SEQUENCE_NUM, tx.ins[0].sequence);
});

test('has the correct inputs hashes', t => {
  t.plan(2);
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(
    tx.ins[0].hash.toString('hex'),
    '5beb0c216fe86cbb3c97ce68f64f2d2f88435c4b8a0879ba691c237eb59aeb5f'
  );
  t.is(
    tx.ins[1].hash.toString('hex'),
    '2c9e3651c7f459ed3e2b14cc417c7369b7a5eafa41b82942808541dcd6e6f6e6'
  );
});


test.todo('has the correct inputs scriptSig');

/*
test('has the correct inputs scriptSig', t => {
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);

  t.is(
    '304402205bdbcb32100b0681616a41b7650691f609f43da7428ed733adbf84ca11bceebd02\
201ae63525e61ad02e703273fe21b3cc7e2de41b2e635cbab14e4bed802c41c00001 OP_TRUE 36\
3332313032626164333734383038363463653064363430656136616438343939393039336536363\
8653733313933643664326663633566346161393631643133643061613961633531623237353637\
3231303364306231386262643463303665653964613636336639303135343666646134636362313\
6313666303962663062333634333035613334303434666636363530306164613931343239613365\
363036323039353332383232643335633562383638363732316462646532663637356138373638',
    bitcoin.script.toASM(tx.ins[0].script)
  );
});
*/

test('has the correct outputs amount', t => {
  const FEE_PER_BYTE = 2000;
  const TX_SIZE = 600;
  const TOTAL_AMOUNT = t.context.data.utxos.reduce(
    (acc, utxo) => acc.add(utxo.amount),
    Btc.zero()
  ).satoshis();
  const AMOUNT = TOTAL_AMOUNT - FEE_PER_BYTE * TX_SIZE;
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(FEE_PER_BYTE),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);
  // console.dir(tx.outs);
  t.is(AMOUNT, tx.outs[0].value);
});

test('has the correct transaction version number', t => {
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);
  t.is(2, tx.version);
});

test('has the correct locktime number', t => {
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );
  const tx = bitcoin.Transaction.fromHex(res.tx);
  t.is(0, tx.locktime);
});

test('correctly set the transaction scriptSig', t => {
  t.plan(2);
  const res = spendRefundTx(
    t.context.data.utxos.map((utxo) =>
      ({
        utxo,
        wif: t.context.data.revPubKey.providerrefundnode.keyPair.toWIF()
      })
    ),
    t.context.data.recipientAddress,
    t.context.data.revPubKey.redeemscript,
    6,
    Btc.fromSatoshis(2000),
    bitcoin.networks.testnet
  );

  const transaction = bitcoin.Transaction.fromHex(res.tx);

  t.is(true, transaction.ins[0].script.length !== 0);
  t.is(true, transaction.ins[1].script.length !== 0);
});
