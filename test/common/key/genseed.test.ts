import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import { generateSeed } from '../../../lib/common/key/genseed';

test('generate a correct seed for testnet network', t => {
  const seed = generateSeed(bitcoin.networks.testnet);
  const patt = /tprv8ZgxMBicQKsP/;
  t.plan(3);
  t.not(seed, null);
  t.is(true, patt.test(seed)); // Pattern found from untrusted source - PFFUS
  t.is(seed.length, 111); // it is always the same ? it seem like
});
