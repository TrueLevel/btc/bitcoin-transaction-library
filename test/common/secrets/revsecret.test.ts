import test from 'ava';
import { HDNode, ECPair, networks } from 'bitcoinjs-lib';
import { deriveRevSecret } from '../../../lib/common/secrets/revsecret';
import { I, N, newI, newN } from '../../../lib/state/state';

test.beforeEach(t => {
  t.context.data = {
    cRoot:
    'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eAnDY8r\
BcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    account: 2,
    n: <N> newN(0, Infinity),
    i: <I> newI(0, Infinity),
  };
});

test('generate the correct secret for i and n', t => {
  t.plan(16);
  const clientNode = HDNode.fromBase58(
    t.context.data.cRoot,
    networks.testnet
  );
  const secret = deriveRevSecret(
    clientNode
      .deriveHardened(44)
      .deriveHardened(0)
      .deriveHardened(t.context.data.account),
    t.context.data.i,
    t.context.data.n
  );

  t.is(
    'c829ea7f30e555902c0df5ea01aaea1907a909446d22f002f429966d4a8613cf',
    secret.toString('hex'));
  t.not(
    '5a9d0a29c951c05d02bb7f1bff66d8bb2ae2ce98e8c176be11a0913cec3ccb64',
    secret.toString('hex'));
  t.not(
    '2470f5c2cb0fbe98c2bea6dbb2558e03f2e014a46a2c17fc08d6b5d30cd1504c',
    secret.toString('hex'));
  t.not(
    '9f02315fd8fe0e971c0f6a5da1fdf47f277bf8f522ee94e858164be9cc2cf356',
    secret.toString('hex'));
  const secret2 = deriveRevSecret(
    clientNode
      .deriveHardened(44)
      .deriveHardened(0)
      .deriveHardened(t.context.data.account),
    t.context.data.i,
    t.context.data.n.next()
  );

  t.not(
    '5ebab23fdcf3d19a7b9ab500efec6589a5b4a3dde71e09b14d0633b494bf76cf',
    secret2.toString('hex'));
  t.is(
    'caf6b825c2d4b29ef5f57cb893287d848b77037d595ddfb8221439d09c927be1',
    secret2.toString('hex'));
  t.not(
    '2470f5c2cb0fbe98c2bea6dbb2558e03f2e014a46a2c17fc08d6b5d30cd1504c',
    secret2.toString('hex'));
  t.not(
    '9f02315fd8fe0e971c0f6a5da1fdf47f277bf8f522ee94e858164be9cc2cf356',
    secret2.toString('hex'));
  const secret3 = deriveRevSecret(
    clientNode
      .deriveHardened(44)
      .deriveHardened(0)
      .deriveHardened(t.context.data.account),
    t.context.data.i.next(),
    t.context.data.n
  );

  t.not(
    '5ebab23fdcf3d19a7b9ab500efec6589a5b4a3dde71e09b14d0633b494bf76cf',
    secret3.toString('hex'));
  t.not(
    '5a9d0a29c951c05d02bb7f1bff66d8bb2ae2ce98e8c176be11a0913cec3ccb64',
    secret3.toString('hex'));
  t.is(
    'a965d03b94edfbcf387f2924e591b55df624c93ff4f071f278de4f8f2ab21b0b',
    secret3.toString('hex'));
  t.not(
    '9f02315fd8fe0e971c0f6a5da1fdf47f277bf8f522ee94e858164be9cc2cf356',
    secret3.toString('hex'));
  const secret4 = deriveRevSecret(
    clientNode
      .deriveHardened(44)
      .deriveHardened(0)
      .deriveHardened(t.context.data.account),
    t.context.data.i.next(),
    t.context.data.n.next()
  );

  t.not(
    '5ebab23fdcf3d19a7b9ab500efec6589a5b4a3dde71e09b14d0633b494bf76cf',
    secret4.toString('hex'));
  t.not(
    '5a9d0a29c951c05d02bb7f1bff66d8bb2ae2ce98e8c176be11a0913cec3ccb64',
    secret4.toString('hex'));
  t.not(
    '2470f5c2cb0fbe98c2bea6dbb2558e03f2e014a46a2c17fc08d6b5d30cd1504c',
    secret4.toString('hex'));
  t.is(
    'e4edaa9c828509b8df65385d8d68e819e34a2e640e0203ca4e067fd6e6b0caf6',
    secret4.toString('hex'));
});

test('should not export to WIF format or base58', t => {
  const clientNode = HDNode.fromBase58(
    t.context.data.cRoot,
    networks.testnet
  );
  const secret = deriveRevSecret(
    clientNode
      .deriveHardened(44)
      .deriveHardened(0)
      .deriveHardened(t.context.data.account),
    t.context.data.i,
    t.context.data.n
  );

  try {
    ECPair.fromWIF(secret.toString('hex'), networks.testnet);
    HDNode.fromBase58(secret.toString('hex'), networks.testnet);
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should return an error with non-wif format');
});

test('failed with negative account', t => {
  const clientNode = HDNode.fromBase58(
    t.context.data.cRoot,
    networks.testnet
  );
  try {
    deriveRevSecret(
      clientNode
        .deriveHardened(44)
        .deriveHardened(0)
        .deriveHardened(t.context.data.account * -1),
      t.context.data.i,
      t.context.data.n
    );
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should return an error');
});

test('failed with negative n', t => {
  const clientNode = HDNode.fromBase58(
    t.context.data.cRoot,
    networks.testnet
  );
  try {
    deriveRevSecret(
      clientNode
        .deriveHardened(44)
        .deriveHardened(0)
        .deriveHardened(t.context.data.account),
      t.context.data.i,
      newN(-1)
    );
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should return an error');
});

test('failed with negative i', t => {
  const clientNode = HDNode.fromBase58(
    t.context.data.cRoot,
    networks.testnet
  );
  try {
    deriveRevSecret(
      clientNode
        .deriveHardened(44)
        .deriveHardened(0)
        .deriveHardened(t.context.data.account),
      newI(-1),
      t.context.data.n
    );
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should return an error');
});
