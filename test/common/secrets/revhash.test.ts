import test from 'ava';
import { crypto } from 'bitcoinjs-lib';
import { revHashFromSecret } from '../../../lib/common/secrets/revhash';

test('generate the correct revocation hash for given secret', t => {
  t.plan(2);
  const hash = revHashFromSecret(Buffer.from('mySuperSecret', 'utf8'));
  t.deepEqual(
    crypto.ripemd160(crypto.sha256(Buffer.from('mySuperSecret', 'utf8'))),
    hash
  );
  // Value not verified correctly
  t.is('8203b087434a93d860f0db44eed5c36be2d7e814', hash.toString('hex'));
});

test('failed with empty string', t => {
  try {
    revHashFromSecret(Buffer.from('', 'utf8'));
  } catch (e) {
    t.pass();
    return;
  }
  t.fail('should return an error');
});
