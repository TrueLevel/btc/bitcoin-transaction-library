import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import { revHashFromSecret } from '../../../lib/common/secrets/revhash';
import { deriveRevSecret } from '../../../lib/common/secrets/revsecret';
import { deriveRevPubkey } from '../../../lib/common/address/deriverevpubkey';
import { newI, newN } from '../../../lib/state/state';
import { HDNode } from 'bitcoinjs-lib';
import { ClientProviderPubkeys } from '../../../lib/common/types';

const deriveUserNode = (node: HDNode, i: number) =>
  node.derive(0).derive(i);

const getPubkeys = (
  clientAcc: HDNode,
  providerAcc: HDNode,
  i: number,

): ClientProviderPubkeys =>
  ({
    clientPubkey: deriveUserNode(clientAcc, i).getPublicKeyBuffer(),
    providerPubkey: deriveUserNode(providerAcc, i).getPublicKeyBuffer(),
  });


test.beforeEach(t => {
  t.context.data = {
    cRoot:
    'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eA\
nDY8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
    'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1\
XiWhg9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    account: 2,
    n: newN(0),
    i: newI(0),
  };
});

test('a well formed output redeem script', t => {
  const clientNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const providerNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const hash = revHashFromSecret(
    deriveRevSecret(
      clientNode
        .deriveHardened(44)
        .deriveHardened(0)
        .deriveHardened(t.context.data.account),
      t.context.data.i,
      t.context.data.n
    )
  );
  const drpk = deriveRevPubkey(
    getPubkeys(clientNode, providerNode, 0),
    hash,
    1,
    bitcoin.networks.testnet
  );

  t.is(
    drpk.redeemScript.toString('hex'),
    '632102bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa9ac51\
b275672102bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa9ada91\
4ca169276509baf590ac7a799aab99df7d404104a8768'
  );
});

test('has well formed address', t => {
  const clientNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );

  const providerNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );

  const hash = revHashFromSecret(
    deriveRevSecret(
      clientNode
        .deriveHardened(44)
        .deriveHardened(0)
        .deriveHardened(t.context.data.account),
      t.context.data.i,
      t.context.data.n
    )
  );
  const drpk = deriveRevPubkey(
    getPubkeys(clientNode, providerNode, 0),
    hash,
    1,
    bitcoin.networks.testnet
  );

  t.is(
    drpk.address,
    '2NCSvNagK5Fgk6R4pe3PJJQvu6mtX1qG3uk',
    'as a valid address'
  );
});

test('has the right client refund node', t => {
  const clientNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  t.is(
    deriveUserNode(clientNode, 0).toBase58(),
    'tprv8eB1MqrLJYZXhmx54htGwNNFPZhp6sS5FzJpPm9qfbxPLQbHRged9JJzed8a45aYQrDSHc\
9rrpS8ddQ3prFM3pSssE1eTzyXzc8g21PMNrm',
    'As the right client refund node'
  );
});

test('has the right provider refund node', t => {
  const providerNode = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  t.is(
    deriveUserNode(providerNode, 0).toBase58(),
    'tprv8eB1MqrLJYZXhmx54htGwNNFPZhp6sS5FzJpPm9qfbxPLQbHRged9JJzed8a45aYQrDSHc\
9rrpS8ddQ3prFM3pSssE1eTzyXzc8g21PMNrm',
    'As the right provider refund node'
  );
});
