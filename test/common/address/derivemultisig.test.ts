import test from 'ava';
import * as bitcoin from 'bitcoinjs-lib';
import deriveChannelAdd from '../../../lib/common/address/derivemultisig';

import { HDNode } from 'bitcoinjs-lib';
import { ClientProviderPubkeys } from '../../../lib/common/types';

const deriveUserNode = (node: HDNode, i: number) =>
  node.derive(0).derive(i);

const getPubkeys = (
  clientAcc: HDNode,
  providerAcc: HDNode,
  i: number,

): ClientProviderPubkeys =>
  ({
    clientPubkey: deriveUserNode(clientAcc, i).getPublicKeyBuffer(),
    providerPubkey: deriveUserNode(providerAcc, i).getPublicKeyBuffer(),
  });


test.beforeEach(t => {
  t.context.data = {
    cRoot:
      'tprv8ZgxMBicQKsPe8a4L4m3rYR6M1NEf5XR3GvcM6fDtLncu5UsGRZ2neSfV78J6eA\
nDY8rBcfBgEfr3LisyMuo6EU6XqgBtoNE3yY94PnTfbU',
    pRoot:
      'tprv8ZgxMBicQKsPeXroNi4Exy94Wvogq4TazKNG4v1R89UqB4QXkddxMYaCCqFcHr1\
XiWhg9Ln2s7iWSgaFgRVe2a257NiZBF7vmBHd1uidd8h',
    account: 2,
    n: 0,
    i: 0
  };
});

test('as well formed output address', t => {
  const clientAcc = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const providerAcc = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );

  const drca = deriveChannelAdd(
    getPubkeys(clientAcc, providerAcc, t.context.data.i),
    bitcoin.networks.testnet
  );
  t.is(
    drca.redeemScript.toString('hex'),
    '522102bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa92102\
bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa952ae'
  );
});

test('has a well formed 2-of-2 multisig redeem script', t => {
  const clientAcc = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  const providerAcc = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );

  const drca = deriveChannelAdd(
    getPubkeys(clientAcc, providerAcc, t.context.data.i),
    bitcoin.networks.testnet
  );
  t.is(
    drca.redeemScript.toString('hex'),
    '522102bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa92102\
bad37480864ce0d640ea6ad84999093e668e73193d6d2fcc5f4aa961d13d0aa952ae'
  );
});

test('has well formed client node', t => {
  const clientAcc = bitcoin.HDNode.fromBase58(
    t.context.data.cRoot,
    bitcoin.networks.testnet
  );
  t.is(
    clientAcc.derive(0).derive(t.context.data.i).toBase58(),
    'tprv8eB1MqrLJYZXhmx54htGwNNFPZhp6sS5FzJpPm9qfbxPLQbHRged9JJzed8a45aYQrDSHc\
9rrpS8ddQ3prFM3pSssE1eTzyXzc8g21PMNrm',
    'as a well formed client node'
  );
});
