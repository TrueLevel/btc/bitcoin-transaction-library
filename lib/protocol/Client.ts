import { TransactionBuilder } from 'bitcoinjs-lib';
import { Btc } from '../common/btc';
import { Protocol } from './Protocol';
import { refundTransaction } from '../common/transaction/refundtx';
import { fundingTransaction } from '../common/transaction/fundingtx';
import {
  Tx,
  Utxo,
  // ApiName,
  UtxoAndWif,
  // NetworkName,
  ChannelTx,
  ContractType,
} from '../common/types';
import { spendRefundTransaction } from '../common/transaction/spendrefundtx';
import { I, N } from '../state/state';
import { Kernel } from '../kernel/Kernel';

/**
 * This class interact with the channel server and expose all methods to create,
 * verify and sign the transaction. The methods are used by the client side of
 * the one way channel.
 */
export class Client extends Protocol {
  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTOR
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Create channel used to interact between the client and server.
   */
  constructor() {
    super();
  }


  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE FUNCTIONS
  //////////////////////////////////////////////////////////////////////////////
  /**
   * Generate the funding transaction that consume all the utxos for the given
   * funding address index and the correct refund transaction attached to it.
   *
   * @param {UtxoAndWif[]} utxosAndWifs an array with properties utxo and the
   * corresponding WIF that unlocks the respective utxo used to fund the channel
   * @param {I} i the index i used in the multisig address
   * @param {N} n the secret index n used to derive the revokation secret
   * @param {Btc} feePerByte the amount of fee per bytes
   * @param {string} providerAddress the address of the provider used in the
   * rebPubKey contract (in this case the provider amount is zero, so the
   * address is not used in the revPubKey contract)
   * @param {Kernel} the kernel to use for the transaction
   * @return {Object} the funding transaction fully signed, the refund
   * transaction completly unsigned and the revokation secret/hash.
   */
  public createFundingTransaction = (
    utxosAndWifs: UtxoAndWif[],
    i: I,
    n: N,
    feePerByte: Btc,
    providerAddress: string,
    kernel: Kernel,
  ): CreateFundingTransaction => {
    // Verify that the needed arguments are all present
    if (
      [utxosAndWifs, i, n, feePerByte, providerAddress]
        .some((arg) => arg === undefined)
    ) {
      throw new Error('Arguments are required!');
    }

    // Derive the channel address used to open the channel
    // In normal case the i is equal to zero, but can be anything else
    const multisig =
      kernel.multisig.deriveContract(i);

    // // Compute the secret n and the hash used in the revPubKey contract
    // const revSecret = this.kernel.revPubkey.deriveRevSecret(i, n);
    // const revHash = revHashFromSecret(revSecret);

    // We use revPubkey's own derivation path to prevent reusing the same keys
    // which signs the multisig transactions (quantum safe)
    const revPubkey =
      kernel.revPubkey.deriveContract(i, n);

    // Generate the funding transaction
    const fundingTx =
      fundingTransaction(
        utxosAndWifs,
        multisig.address,
        feePerByte,
        kernel.getNetwork()
      );

    // Create the refund transaction with the funding transaction utxos. All the
    // channel amount is send to the revPubKey contract, the amount of the
    // provider is set to zero, no transaction have been created yet.
    const refundTx =
      refundTransaction(
        fundingTx.utxos,
        revPubkey.address,
        Btc.zero(), // providerBalance
        fundingTx.channelState.clientBalance.afterBroadcast, // clientBalance
        feePerByte,
        providerAddress,
        kernel.getNetwork(),
      );
    return {
      fundingTx,
      refundTx,
      revPubkey,
      multisig,
    };
  };


  /**
   * Sign the given refund transaction with the correct node for the given
   * index.
   *
   * @param {string | TransactionBuilder} transactionHex transaction
   * in hex format
   * @param {I} i the index i used in the multisig address
   * @param {Kernel} the kernel to use for the transaction
   */
  public cosignRefundTransaction = (
    transactionHex: string | TransactionBuilder,
    i: I,
    kernel: Kernel,
  ): TransactionBuilder => {
    // Verify that the needed arguments are all present
    if ([transactionHex, i].some((arg) => arg === undefined)) {
      throw new Error('Arguments are required!');
    }

    // Reconstruct the transaction and the transaction builder
    const builder = this.builderFromTransactionHex(transactionHex, kernel);
    // TODO verify channel address
    // TODO verify revPubKey contract i
    // TODO verify secret (get n in arguments)

    const multisig = kernel.multisig.deriveContract(i);
    // Sign the first input of the transaction with the client private node
    builder.sign(
      0,
      kernel.multisig.getKeyPair(i),
      multisig.redeemScript
    );

    // Return the new signed transaction to hex format
    return builder;
  };

  /**
   * Create a fully signed transaction that consume all the utxos for a
   * revPubKey contract of a refund transaction. This transaction can be create
   * right after the refund transaction is broadcast but is valid only after
   * the timelock present in the revPubKey contract.
   *
   * @param {UtxoAndWif[]} utxosAndWifs an array of Utxos and wif keys
   * @param {I} i the index i, index of the multisig
   * @param {N} n the secret index n used to derive the revokation secret
   * @param {Btc} feePerByte the amount of fee per bytes used to create the
   * transaction
   * @param {string} recipientAddress the address where the funds is sends to
   * @param {Kernel} the kernel to use for the transaction
   * @return {Tx} a channel transaction containing the spend refund transaction
   */
  public createSpendRefundTransaction = (
    utxosAndWifs: UtxoAndWif[],
    i: I,
    n: N,
    feePerByte: Btc,
    recipientAddress: string,
    kernel: Kernel,
  ): Tx => {
    // Verify that all arguments are present
    if (
      [utxosAndWifs, i, n, feePerByte, recipientAddress]
        .some((arg) => arg === undefined)
    ) {
      throw new Error('Arguments are required!');
    }

    // we use the pubkey of the next (unused) multisig address for the revPubKey
    // contract to prevent reusing the same key which signs the transaction to
    // the revPubKey address (quantum safe)
    const revPubkey = kernel.revPubkey.deriveContract(
      i,
      n,
    );

    return spendRefundTransaction(
      utxosAndWifs,
      recipientAddress,
      revPubkey.redeemScript,
      kernel.getBlocksInFuture(),
      feePerByte,
      kernel.getNetwork(),
    );
  };

  /**
   * Initiate the protocol to send an amount of satoshis to the provider. This
   * method create the refund transaction with the new balance. The new refund
   * transaction used the revPubkey contract for the remaining funds in the
   * channel. The revPubkey contract is created with i and n. The new revocation
   * secret and the associated hash is created for the state n.
   *
   * @param {Utxos[]} utxos the utxos used to create the refund. These utxos has
   * to come from the multisig address i
   * @param {I} i the index i for the multisig address, index of the utxos
   * @param {N} n the secret index n, used to derive the secret
   * @param {Btc} providerBalance the amount that goes to the provider
   * @param {Btc} clientBalance the amount that goes into the revPubKey
   * @param {Btc} feePerByte the amount of fee used to create the transactions
   * @param {string} providerAddress the address used to send funds to the
   * provider
   * @param {Kernel} the kernel to use for the transaction
   * @return {InitiateTransaction} the refund transaction, the revPubKey
   * contract, the revokaction secret and the hash
   */
  public initiateTransaction = (
    utxos: Utxo[],
    i: I,
    n: N,
    providerBalance: Btc,
    clientBalance: Btc,
    feePerByte: Btc,
    providerAddress: string,
    kernel: Kernel,
  ): InitiateTransaction => {
    // Verify that all arguments are present
    if (
      [utxos, i, n, providerBalance, clientBalance, feePerByte, providerAddress]
        .some((arg) => arg === undefined)
    ) {
      throw new Error('Arguments are required!');
    }

    const revPubkey = kernel
      .revPubkey.deriveContract(i, n);

    const previousSecret = kernel
      .revPubkey.deriveRevSecret(i, n.previous());

    const refundTx = refundTransaction(
      utxos,
      revPubkey.address,
      providerBalance,
      clientBalance,
      feePerByte,
      providerAddress,
      kernel.getNetwork()
    );

    return {
      refundTx,
      revPubkey,
      previousSecret,
    };
  };



  /**
   * Sign the settlement transaction for a given state i and n. The settlement
   * transaction spend the funds to the next multisig address and the provider
   * balance to the provider. When the client sign the settlement transaction,
   * he must share the previous revokation hash, for the state n - 1.
   *
   * @param {I} i the index i for the current multisig address
   * @param {N} n the index n for the current sercret, the share secret is
   * n - 1
   * @param {string} settlementTxHex the settlement transaction in hex format
   * @param {Kernel} the kernel to use for the transaction
   * @return {any} the sign settlement transaction and the previous revokation
   * secret
   */
  public signSettlementTransaction = (
    i: I,
    n: N,
    settlementTxHex: string,
    kernel: Kernel,
  ) => {
    // Verify that all arguments are present
    if ([i, n, settlementTxHex].some((arg) => arg === undefined)) {
      throw new Error('Arguments are required!');
    }

    // TODO verify amount and outputs

    // Reconstruct the transaction and the transaction builder
    const builder = this.builderFromTransactionHex(settlementTxHex, kernel);

    const multisig = kernel.multisig.deriveContract(i);

    // Sign the first input of the transaction with the client private node
    builder.sign(
      0,
      kernel.multisig.getKeyPair(i),
      multisig.redeemScript
    );

    // Compute the previous revocation secret to share it with the server
    const previousRevSecret = kernel
      .revPubkey.deriveRevSecret(i, n.previous());

    return {
      settlementTxHex: builder.build().toHex(),
      previousRevSecret
    };
  };

  /**
   * Sign the post settlement transaction for the state i. The post settlement
   * transaction is a refund transaction for a settlement transaction. This
   * method use the sign refund transaction with the next multisig i to sign the
   * transaction.
   *
   * @param {string} postSettlementRefundTxHex the transaction in hex format
   * @param {I} i the current index i
   * @param {Kernel} the kernel to use for the transaction
   * @return {TransactionBuilder} the signed transaction to hex format
   */
  public signPostSettlementRefundTransaction = (
    postSettlementRefundTxHex: string,
    i: I,
    kernel: Kernel,
  ): TransactionBuilder =>
    this.cosignRefundTransaction(postSettlementRefundTxHex, i.next(), kernel);

}

export interface CreateFundingTransaction {
  fundingTx: ChannelTx;
  refundTx: ChannelTx;
  revPubkey: ContractType & { revHash: Buffer };
  multisig: ContractType;
}

export interface InitiateTransaction {
  refundTx: ChannelTx;
  revPubkey: ContractType & { revHash: Buffer };
  previousSecret: Buffer;
}
