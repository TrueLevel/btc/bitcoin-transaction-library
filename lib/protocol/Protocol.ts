import {
  Network,
  networks,
  TransactionBuilder,
  Transaction,
  HDNode,
} from 'bitcoinjs-lib';
import { generateSeed } from '../common/key/genseed';
import { revHashFromSecret } from '../common/secrets/revhash';
import {
  NetworkName,
  // ApiName,
  networkName,
  UtxoAndWif,
  Utxo,
} from '../common/types';
import { Kernel } from '../kernel/Kernel';
import { derivation } from '../common/node/derivation';

/**
 * This class implements the basic capabilites for the client protocol and the
 * server protocol.
 */
export class Protocol {

  //////////////////////////////////////////////////////////////////////////////
  // STATIC FUNCTIONS
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Generate a seed for the given network.
   *
   * @param {Network} network the network used to generate a node and serialize
   * it
   * @return {string} the seed
   */
  public static generateSeed = (network: Network = networks.testnet): string =>
    generateSeed(network);

  /**
   * Generate a new account for the user, for a given accountId and a specific
   * network.
   *
   * @param {number} accountId
   * @param {Network} network the network used to generate a node and serialize
   * it
   * @return {Object} Object containing private key and account public key
   */
  public static createAccKeys = (
    accountId: number,
    network: Network = networks.testnet,
  ) => {
    const privKey = Protocol.generateSeed(network);
    const privNode = HDNode.fromBase58(privKey, network);
    const privAcc = derivation.privateAccount(privNode, accountId);
    const pubAcc = privAcc.neutered().toBase58();
    return {
      privKey,
      pubAcc, // to share with other party
    };
  }

  public static networkFromString = (net: NetworkName) => {
    if (networkName.every((network) => network !== net)) {
      throw new Error(`Unsupported network ${net}`);
    }
    return networks[
      net === 'regtest'
        ? 'testnet'
        : net
    ];
  }


  public createUtxosAndWifs = (
    utxos: Utxo[],
    wif: string,
  ): UtxoAndWif[] =>
    utxos.map((utxo) =>
      ({
        utxo,
        wif,
      }));
  /**
   * Compute the hash from a secret.
   *
   * @param {string} s the secret to hash
   * it
   * @return {string} the reuslted hash
   */
  static revHashFromSecret = (s: string): string =>
    revHashFromSecret(
      Buffer.from(s, 'utf8')
    ).toString('hex');

  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE FUNCTIONS
  //////////////////////////////////////////////////////////////////////////////
  /**
    * @param {string | TransactionBuilder} the hex of the transaction
    * @param {Kernel} the kernel of the channel
    */

  public builderFromTransactionHex = (
    transactionHex: string | TransactionBuilder,
    kernel: Kernel,
  ): TransactionBuilder => {
    if (typeof transactionHex === 'string') {
      const transaction = Transaction.fromHex(transactionHex);
      return TransactionBuilder
        .fromTransaction(transaction, kernel.getNetwork());
    } else { return transactionHex; }
  }

}
