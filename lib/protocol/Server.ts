import { Protocol } from './Protocol';
import { revHashFromSecret } from '../common/secrets/revhash';
import { refundTransaction } from '../common/transaction/refundtx';
import {
  Tx,
  Btc,
  Utxo,
  UtxoAndWif,
  // ApiName,
  // NetworkName,
  ContractType
} from '../common/types';
import { settlementTransaction } from '../common/transaction/settlementtx';
import { revokeRefundTransaction } from '../common/transaction/revokerefundtx';
import { TransactionBuilder } from 'bitcoinjs-lib';
import { I, N } from '../state/state';
import { Kernel } from '../kernel/Kernel';
/**
 * The server channel class can interact with an one way channel as a server.
 * This class expose all the necessary methods to create, verify and sign all
 * the possible transactions for the channel.
 */
export class Server extends Protocol {

  //////////////////////////////////////////////////////////////////////////////
  // PUBLIC PROPERTIES
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTOR
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Create channel used to interact between the client and server.
   *
   */
  constructor() {
    super();
  }


  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE FUNCTIONS
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Sign a refund transaction. For first refund transaction we do not have
   * the signature of funding transaction so cannot verify it. For all future
   * refund transactions the signatures of the transaction inputs should be
   * verified.
   *
   * @param {string | TransactionBuilder} transactionHex the raw transaction
   * to hex format
   * @param {I} i the index i used to create the multisig index
   * @param {Kernel} the kernel to use for the transaction
   * @return {TransactionBuilder} the signed transaction to hex format
   */
  public signRefundTransaction = (
    transactionHex: string | TransactionBuilder,
    i: I,
    kernel: Kernel,
  ): TransactionBuilder => {

    // Verify that the needed arguments are all present
    if ([transactionHex, i].some((arg) => arg === undefined)) {
      throw new Error('Arguments are required!');
    }

    // FIXME: add this back after tests are done
    // // we use the pubkey of the next (unused) multisig address for the
    // rebpubkey contract to prevent reusing the same key which signs the
    // transaction to the revpubkey address (quantum safe)

    // const revPubKey = deriveRevPubkey(
    //   this.channel._clientAccPub,
    //   this.channel._serverAccPriv,
    //   i + 1,
    //   Buffer.from(secretHash, 'hex'),
    //   blocksInFuture,
    //   this.channel._network
    // );
    // // Make sure the transaction goes to the right revPubKey address
    // const addressToVerify = address.fromOutputScript(
    //   tx.outs[0].script,
    //   this.channel._network
    // );
    // if (addressToVerify.toString() !== revPubKey.address.toString()) {
    //   throw new Error(`Output address does not match expected address`);
    // }

    // Sign the transaction with the private key

    // Derive the keys to recreate the multisig redeem script
    // Retreive the 2-of-2 multisig redeem script used in the multisig address

    const builder = this.builderFromTransactionHex(transactionHex, kernel);
    const multisig = kernel.multisig.deriveContract(i);
    builder.sign(
      0,
      kernel.multisig.getKeyPair(i),
      multisig.redeemScript
    );

    return builder;
  };

  /**
   * This method is used to cosign to the settlement transaction. The method is
   * used by the server to fully sign the settlement transaction before
   * broadcast it. The transaction is sign for the state i. The state i is used
   * to reconstruct the multisig contract.
   *
   * @param {string} transaction the raw transaction in hex format, already
   * signed by the client
   * @param {I} i the index i used to recreate the multisig address
   * @param {Kernel} the kernel to use for the transaction
   * @return {string} the fully sign settlement transaction
   */
  public cosignSettlementTransaction = (
    transactionHex: string,
    i: I,
    kernel: Kernel
  ): string => {

    // Verify that the needed arguments are all present
    if ([transactionHex, i].some((arg) => arg === undefined)) {
      throw new Error('Arguments are required!');
    }

    const multisig = kernel.multisig.deriveContract(i);
    // Derive the keys to recreate the multisig redeem script
    // const clientKey = this._clientAccPub.derive(0).derive(i);
    // const serverKey = this._serverAccPriv.derive(0).derive(i);

    // Retreive the 2-of-2 multisig redeem script used in the multisig address
    // const redeemScript = script.multisig.output.encode(2, [
    //   clientKey.getPublicKeyBuffer(),
    //   serverKey.getPublicKeyBuffer()
    // ]);
    const builder = this.builderFromTransactionHex(transactionHex, kernel);

    // Sign the first input of the transaction with the client private node
    builder.sign(
      0,
      kernel.multisig.getKeyPair(i),
      multisig.redeemScript
    );

    return builder.buildIncomplete().toHex();
  };

  /**
   * Create a revokation transaction if the client broadcast a old refund
   * transaction. The transaction spend all the utxos present in the multisig
   * for index i and send it to the settlement address. This transaction is
   * valid only if the server knows the revokation secret.
   *
   * @param {UtxoAndWif[]} utxosAndWifs an array of Utxos and wif keys
   * @param {I} i the index i used to create the multisig index
   * @param {Btc} feePerByte the amount of fee per byte for the crafted
   * transactions
   * @param {string} recipientAddress the address to send the funds present
   * into the revPubKey contract
   * @param {Buffer} revSecret the secret used in the revPubKey contract
   * @param {Kernel} the kernel to use for the transaction
   * @return {Tx} a channel transaction containing the revoke refund transaction
   * fully signed
   */
  public createRevokeRefund = (
    utxosAndWifs: UtxoAndWif[],
    i: I,
    feePerByte: Btc,
    recipientAddress: string,
    revSecret: Buffer,
    kernel: Kernel,
  ): Tx => {
    // Verify that all arguments are present
    if (
      [utxosAndWifs, i, feePerByte, recipientAddress, revSecret]
        .some((arg) => arg === undefined)
    ) {
      throw new Error('Arguments are required!');
    }
    // Compute the revokation hash from the revokation secret
    const revHash = revHashFromSecret(revSecret);

    // we use the pubkey of the next (unused) multisig address for the rebpubkey
    // contract to prevent reusing the same key which signs the transaction to
    // the revpubkey address (quantum safe)
    const revPubKey = kernel.revPubkey.deriveContract(
      i,
      revHash,
    );

    return revokeRefundTransaction(
      utxosAndWifs,
      recipientAddress,
      revPubKey.redeemScript,
      revSecret,
      feePerByte,
      kernel.getNetwork(),
    );
  };

  /**
   * Generate the settlement and post settlement transaction for the given
   * refund when the client want to send an amount to the provider. The server
   * receive the refund transaction with an amount and create the settlement
   * and post settlement transaction with the right i and n. The server return
   * the settlement, post settlement and the signed refund transactions with
   * the next this.channel address.
   *
   * @param {Utxo[]} utxos the utxos used to create the settlement transaction,
   * must be the same as the refund transaction
   * @param {I} i the current index i, used to unlock the utxos
   * @param {N} n the secret index n
   * @param {string} refund the refund transaction in hex format
   * @param {string} revHash the revokation hash generated by the client
   * @param {string} providerAddress the address used to send the funds to the
   * provider
   * @param {Btc} settlementAmount the amount send to the provider
   * @param {Btc} changeAmount the amount send to the revPubKey contract
   * @param {Btc} feePerByte the amount of fee used to create the transactions
   * @param {Kernel} the kernel to use for the transaction
   * @return {CreateSettlementTransaction} the signed refund transaction,
   * the settlement, the post settlement transaction and the next multisig
   * address
   */
  public createSettlementTransaction = (
    utxos: Utxo[],
    i: I,
    n: N,
    refundtxHex: string,
    revHashString: string,
    providerAddress: string,
    settlementAmount: Btc,
    changeAmount: Btc,
    feePerByte: Btc,
    kernel: Kernel,
  ): CreateSettlementTransaction => {

    const nextMultisig =
      kernel.multisig.deriveContract(i.next());

    const nextRevPubkey = kernel.revPubkey.deriveContract(
      i.next(),
      Buffer.from(revHashString),
    );

    const settlementTx = settlementTransaction(
      utxos,
      providerAddress,
      settlementAmount,
      nextMultisig.address,
      changeAmount,
      feePerByte,
      kernel.getNetwork(),
    );

    const postSettlementRefundTx = refundTransaction(
      settlementTx.utxos
        .filter((utxo) =>
          nextMultisig.address === utxo.address
        ),
      nextRevPubkey.address,
      Btc.zero(),
      settlementTx.channelState.clientBalance.afterBroadcast,
      feePerByte,
      providerAddress,
      kernel.getNetwork(),
    );

    // Sign refund transaction
    const refundTxProviderHex =
      this.signRefundTransaction(
        refundtxHex,
        i,
        kernel,
      ).buildIncomplete().toHex();

    // Sign the transaction with the private key
    postSettlementRefundTx.builder.sign(
      0,
      kernel.multisig.getKeyPair(i.next()),
      nextMultisig.redeemScript
    );

    return {
      refundTxHex: refundTxProviderHex,
      settlementTxHex: settlementTx.tx,
      postSettlementTxHex: postSettlementRefundTx
        .builder.buildIncomplete().toHex(),
      nextMultisig,
    };
  };
}

export interface CreateSettlementTransaction {
  refundTxHex: string;
  settlementTxHex: string;
  postSettlementTxHex: string;
  nextMultisig: ContractType;
}
