import { I, N, newN, newI, newT, newState, State } from '../state/state';
import {
  Btc,
  NetworkName,
  ApiName,
  UtxoAndWif,
} from '../common/types';
import { Client } from '../protocol/Client';
import { Server } from '../protocol/Server';
import { Protocol } from '../protocol/Protocol';
import { Kernel } from '../kernel/Kernel';
import { TransactionBuilder } from 'bitcoinjs-lib';

/// client and server mixed, separate them afterwards on extended classes, and
/// abstract commonalities on this one class
export class StatefulChannel {
  state: State;
  clientBalance: Btc = Btc.zero();
  channelTotal: Btc = Btc.zero();
  serverBalance: Btc = Btc.zero();
  server: Server;
  serverKernel: Kernel;
  client: Client;
  clientKernel: Kernel;
  protocol: Protocol;
  protocolKernel: Kernel;
  feePerByte: Btc;
  serverAddress: string;
  funding: TransactionBuilder;
  refund: TransactionBuilder;
  revHash: Buffer[];
  threshold = 7; // threshold for t

  constructor(
    clientKey: string,
    serverKey: string,
    accountId: number,
    net: NetworkName,
    api: ApiName,
    blocksInFuture: number,
  ) {
    this.client = new Client(
      // clientKey,
      // serverKey,
      // accountId,
      // net,
      // api,
      // blocksInFuture,
    );
    this.clientKernel = new Kernel(
      clientKey,
      serverKey,
      accountId,
      net,
      api,
      blocksInFuture,
    );
    this.server = new Server(
      // clientKey,
      // serverKey,
      // accountId,
      // net,
      // api,
      // blocksInFuture,
    );
    this.serverKernel = new Kernel(
      clientKey,
      serverKey,
      accountId,
      net,
      api,
      blocksInFuture,
    );
    this.protocol = new Protocol(
      // clientKey,
      // serverKey,
      // accountId,
      // net,
      // api,
      // blocksInFuture,
    );
    this.protocolKernel = new Kernel(
      clientKey,
      serverKey,
      accountId,
      net,
      api,
      blocksInFuture,
    );
    this.onStart();

  }

  public onStart = async () => {
    this.feePerByte = await this.getFeePerByte(6);
    // this.setProviderAddress();
  }

  public getFeePerByte = async (blocks: number) =>
    await this.clientKernel.getAdapter().getEstimateFee(blocks);

  public setFeePerByte = (feePerByte: Btc) =>
    this.feePerByte = feePerByte;

  public setProviderAddress = (providerAddress: string) =>
    this.serverAddress = providerAddress;


  // public setInitialState = (i: number, n: number, t: number) =>
  //   this.state = newState(newI(i), newN(n), newT(t, this.threshold));

  // it starts with the state variable uninitialized

  // state 0 client share refund to provider, provider signs it (and move to
  // state 1) and sends it back to client. client now broadcasts it and move to
  // state 1 as well


  public state0Client = (
    utxosAndWifs: UtxoAndWif[],
    i = newI(0, Infinity),
    n = newN(0, Infinity),
  ) => {
    if (this.state) { throw new Error('State is already set!'); }

    const txs = this.client.createFundingTransaction(
      utxosAndWifs,
      i,
      n,
      this.feePerByte,
      this.serverAddress,
      this.clientKernel,
    );
    this.funding = txs.fundingTx.builder;

    // (1) verify that refundTx is not signed, (2) share refund with server
    // (3) then update state
    this.refund = txs.refundTx.builder;
    this.revHash = [...this.revHash, txs.revPubkey.revHash];

    const { verifiedScript, verifiedSignature } =
      this.clientKernel.multisig.verifySignature(this.refund, i, 'client');

    if (verifiedScript && verifiedSignature) {
      // sharetx(this.refund.tx.toHex())
      // shareState(i, n)
      this.state = newState(i, n, newT(0, this.threshold));
    } else {
      throw new Error('Client should not have signed yet!');
    }
  }

  public state0Server = (
    refundTxHex: string | TransactionBuilder,
    i: I,
    n: N,
  ) => {
    if (this.state) { throw new Error('State is already set!'); }
    // receive refundtx transaction from client and revHash

    // provider can in general validade every parameter of the transaction
    // except for the revHash, as he cant compute the secret
    const builder = this.server.builderFromTransactionHex(
      refundTxHex,
      this.serverKernel
    );

    // it is safe for the server to sign the refund without checking as he has
    // no funds at stake
    const refundTxServer = this.server.signRefundTransaction(
      builder,
      i,
      this.serverKernel
    );

    // server sends the signed refund transaction to client
    // and updates state
    this.refund = refundTxServer;
    // sharetx(this.refund.tx.toHex())
    this.state = newState(i, n, newT(1, this.threshold));
  }

  public state1Client = async (
    refundTxHexServer: string | TransactionBuilder
  ) => {
    const builder = this.client.builderFromTransactionHex(
      refundTxHexServer,
      this.clientKernel
    );

    // validate sig
    if (
      this.clientKernel.multisig
        .verifySignature(builder, this.state.i, 'server')
    ) {
      try {
        await this.clientKernel.getAdapter()
          .broadcastTx(this.funding.build().toHex());
        this.state = this.state.next.t();
        return true;
      } catch (err) {
        return new Error(`Could not broadcast funding transaction: ${err}`);
      }
    }
  }

  public state2Both = async () => {
    if (!this.state.t.isEqual(1) || !this.state.n.isEqual(0)) {
      return new Error(
        `Channel not in state t = 1, n = 0, \\
current state = ${this.state.toString()}`
      );
    }
    try {
      const channelTotal = await this.updateChannelTotal(this.state.i);
      if (this.channelTotal.greaterThan(Btc.zero())) {
        this.state = this.state.next.t();
      } else {
        return new Error(
          `No funds locked in multig i = ${this.state.i.flatten()}`
        );
      }
      // fetch information about the locked funds in the multisig address and
      // set the total equal to channel
      this.serverBalance = Btc.zero();
      // if n = 0, then all the funds belong to the client
      this.clientBalance = channelTotal;
    } catch (err) { return err; }
    return `Successfully found ${this.channelTotal.bitcoin().toString()} \\
in multisig i = ${this.state.i.flatten()}`;
  }

  public updateChannelTotal = async (i: I) => {
    // call after waiting for funds to get locked on the multisig
    const multisig = this.protocolKernel.multisig.deriveContract(i);
    const utxos = await this.protocolKernel.getAdapter()
      .getUtxos(multisig.address);
    this.channelTotal = utxos.reduce(
      (tot, utxo) => tot.add(utxo.amount),
      Btc.zero()
    );
    return this.channelTotal;
  }

  searchForMultisigWithFunds = () => {
    // not needed and dangerous. to discover where in i did we stop. if we
    // incremented n before losing the state, the provider could revoke the
    // refund transaction. If the client loses the state, he demands the
    // provider to share the n, i, the secret(n-1, i) along with the signed
    // transactions and balances. A malicious provider could cheat and share a
    // past state, and revoke all the locked funds. should store very safely n,
    // i, balances and last transaction set.
  };

  requestLastState = () => {
    // request last state to the other party
  }

  requestConfig = () => {
    // ask for the configurations used by the other party
  }




  // TODO: Transaction checker

  // (1) check other party signature

  // (2) check enough fee has been included in transaction

  // (3) receiver's balance should always increase

  // (4) valid, well formed transaction

  // (5) do sender and receiver agree on each one's balance

  // (6) did receiver share the secret



  // client shares accountId
  // client shares refund, n, i


  // listen to txs published to the blockchain that consume the locked funds

}
