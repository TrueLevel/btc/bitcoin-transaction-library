import { isEqual } from 'lodash';

const isValid = (m: number) =>
  m >= 0
  && m === Math.floor(m)
  && m < 2e12;

export const newN = (n: number, m: number = Infinity): N => {
  if (isValid(n)) {
    return {
      isEqual: (j: number) => n === j,
      toString: () => n.toString(),
      flatten: () => n,
      next: () => (n < m) ? newN(n + 1, m) : newN(n, m),
      n,
      previous: () => newN(n - 1, m),
    };
  } else { throw new Error(`${n} cannot be of type N`); }
};

export const newI = (i: number, m: number = Infinity): I => {
  if (isValid(i)) {
    return {
      isEqual: (j: number) => i === j,
      toString: () => i.toString(),
      flatten: () => i,
      next: () => (i < m) ? newI(i + 1, m) : newI(i, m),
      i,
    };
  } else { throw new Error(`${i} cannot be of type I`); }
};

export const newT = (t: number, m: number): T => {
  if (isValid(t)) {
    return {
      isEqual: (j: number) => t === j,
      toString: () => t.toString(),
      flatten: () => t,
      next: () => (t < m) ? newT(t + 1, m) : newT(t, m),
      m,
      t,
    };
  } else { throw new Error(`${t} cannot be of type T`); }
};

export const newState = (i: I, n: N, t: T): State => ({
  i,
  n,
  t,
  isEqual: (S2: State) => isEqual(newState(i, n, t).flatten(), S2.flatten()),
  toString: () => `(${i.toString()}, ${n.toString()}, ${t.toString()})`,
  flatten: () => ({ i: i.flatten(), n: n.flatten(), t: t.flatten() }),
  next: {
    i: () => {
      const res = (t.m <= t.flatten())
        ? newState(i.next(), n, newT(0, t.m))
        : newState(i, n, t);
      // console.log(`${newS(i, n, t).toString()} -> ${res.toString()}`);
      return res;
    },
    n: () => {
      const res = (t.m <= t.flatten())
        ? newState(i, n.next(), newT(0, t.m))
        : newState(i, n, t);
      // console.log(`${newS(i, n, t).toString()} -> ${res.toString()}`);
      return res;
    },
    t: () => {
      const res = newState(i, n, t.next());
      // console.log(`${newS(i, n, t).toString()} -> ${res.toString()}`);
      return res;
    },
  }
});

interface Common<K> {
  isEqual(N: number): boolean;
  toString(): string;
  flatten(): number;
  next(): K;
}

export interface N extends Common<N> { n: number; previous(): N; }
export interface I extends Common<I> { i: number; }
export interface T extends Common<T> { t: number; m: number; }

export interface State {
  i: I;
  n: N;
  t: T;
  next: {
    i(): State;
    n(): State;
    t(): State;
  };
  isEqual(S: State): boolean;
  toString(): string;
  flatten(): { i: number, n: number, t: number };
}
