import { HDNode } from 'bitcoinjs-lib';
import { I, N } from '../../state/state';

export namespace derivation {
  export const privateAccount = (userRoot: HDNode, accountId: number): HDNode =>
    userRoot
      .deriveHardened(44)
      .deriveHardened(0)
      .deriveHardened(accountId);

  export const multisig = (account: HDNode, i: I): HDNode =>
    account
      .derive(0) // branch chosen for multisig
      .derive(i.flatten());

  export const revPubkey = (account: HDNode, i: I): HDNode =>
    account
      .derive(1) // branch chosen for revpubkey
      .derive(i.flatten());

  export const secret = (clientAccount: HDNode, n: N): HDNode =>
    clientAccount
      .deriveHardened(1e6) // branch chosen for secrets
      .deriveHardened(n.flatten());

  export const funding = (clientRoot: HDNode): HDNode =>
    privateAccount(clientRoot, 1); // accountId 1 is reserved for funding
}
