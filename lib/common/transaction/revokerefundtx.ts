import {
  script,
  ECPair,
  opcodes,
  address,
  Network,
  Transaction,
  TransactionBuilder,
} from 'bitcoinjs-lib';
import { Btc, Tx, UtxoAndWif } from '../types';
import { getOutputs, validBalancesOrThrow } from './helper';

/**
 * Create a revocation transaction that can be used to get the funds blocked
 * in the revPubKey contract with the provider address and the secret. This
 * transaction must be used in the case if the client broadcast an older
 * refund transaction, with a lower balance for the provider. In that case,
 * the provider knows the secret used in the contract and can take the money
 * before the client.
 *
 * @param {UtxoAndWif[]} utxosAndWifs utxos of type revPubKey contract with
 * the corresponding node able to sing the output, must be the same revPubKey
 * contract that the redeemScript argument
 * @param {string} recipientAddress to destination address
 * @param {Buffer} redeemScript the redeem script of the revPubKey
 * @param {Buffer} revSecret the secret to unlock the funds
 * @param {Btc} feePerByte the amount of satoshis per byte
 * @param {Network} network the used network
 * @return {Tx} an object that contains the revoke refund transaction
 */
export const revokeRefundTransaction = (
  utxosAndWifs: UtxoAndWif[],
  recipientAddress: string,
  redeemScript: Buffer,
  revSecret: Buffer,
  feePerByte: Btc,
  network: Network
): Tx => {

  // convert string address into outputscript
  const recipientOutputScript = address
    .toOutputScript(recipientAddress, network);

  // Default signature hash type which signs the entire transaction except any
  // signature scripts, preventing modification of the signed parts.
  // @source https://bitcoin.org/en/glossary/sighash-all
  const HASH_TYPE = Transaction.SIGHASH_ALL;

  // Create a transaction builder with the given network and set the version
  const builder = new TransactionBuilder(network);
  // Note: A transaction version number of 2 does not have the same meaning as
  // in Bitcoin, where it is associated with support for OP CHECKSEQUENCEVERIFY
  // as specified in [BIP-68]. Zcash was forked from Bitcoin v0.11.2 and does
  // not currently support BIP 68, or the related BIPs 9, 112 and 113.
  //
  // @url https://github.com/bitcoin/bips/blob/master/bip-0068.mediawiki
  builder.setVersion(2);

  // Estimate the transaction size and compute the amount of fee
  const estimateSize = 600; // TODO do better
  const fee = feePerByte.mul(estimateSize);

  // Compute the total satoshis present in the utxos and compute the amount
  // send to the recepient address
  const total = utxosAndWifs.reduce(
    (acc, utxoAndWif) =>
      acc.add(utxoAndWif.utxo.amount), Btc.zero()
  );
  const providerBalance = total.sub(fee);

  validBalancesOrThrow(total, providerBalance, fee);

  // Send all the money in the recipient address and add all utxos as inputs
  // Sequence number is set to zero to disable the locktime in the transaction
  //
  // Definition:
  // Part of all transactions. A number intended to allow unconfirmed
  // time-locked transactions to be updated before being finalized; not
  // currently used except to disable locktime in a transaction
  builder.addOutput(recipientOutputScript, providerBalance.satoshis());
  utxosAndWifs.forEach(utxoAndWif => {
    builder.addInput(utxoAndWif.utxo.txid, utxoAndWif.utxo.vout, 0);
  });

  // For each given utxos create the scriptSig and set it in the transaction
  utxosAndWifs.forEach((utxoAndWif, index) => {
    // Generate the hash used to create the scriptSig to unlock the funds in the
    // revPubKey contract
    const signatureHash = builder.tx.hashForSignature(
      index,
      redeemScript,
      HASH_TYPE
    );

    const redeemScriptSig = script.scriptHash.input.encode(
      [
        revSecret,
        ECPair.fromWIF(utxoAndWif.wif, network)
          .sign(signatureHash)
          .toScriptSignature(HASH_TYPE),
        opcodes.OP_FALSE
      ],
      redeemScript
    );

    builder.tx.setInputScript(index, redeemScriptSig);
  });

  return {
    // txid: transaction.getId(),
    tx: builder.tx.toHex(),
    // transaction: transaction,
    builder: builder,
    isComplete: true,
    estimatedSize: estimateSize,
    utxos: getOutputs(builder.tx, network)
  };
};

export default revokeRefundTransaction;
