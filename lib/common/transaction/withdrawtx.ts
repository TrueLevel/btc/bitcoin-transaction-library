import { Utxo, Btc, ChannelTx } from '../types';
import { getOutputs, stateTransition } from './helper';
import { TransactionBuilder, Network, address } from 'bitcoinjs-lib';

/**
 * Create a transaction with three output, one to the provider, one to the
 * client and one to an arbitrary address. This transaction is a cooperative
 * transaction, it means that both part have to be cooperative to create this
 * transaction on demand.
 *
 * @param {Utxo[]} utxos an array of utxos used to create the transaction
 * @param {string} providerAddress the settlement address
 * @param {Btc} providerBalance the settlement amount
 * @param {string} withdrawAddress the withdraw address
 * @param {Btc} withdrawAmount the withdraw amount
 * @param {string} changeAddress the next channel address
 * @param {Btc} changeAmount the amount due to the client on the next address
 * @param {Btc} feePerByte the amount of satoshis per byte of tx
 * @param {Network} network the network used to create the transaction
 * @return {ChannelTx} an object that contains the transaction
 */
export const withdrawTransaction = (
  utxos: Utxo[],
  providerAddress: string,
  providerBalance: Btc,
  withdrawAddress: string,
  withdrawAmount: Btc,
  changeAddress: string,
  changeAmount: Btc,
  feePerByte: Btc,
  network: Network
): ChannelTx => {
  // Convert string address to Buffer script
  const providerOutputScript = address
    .toOutputScript(providerAddress, network);
  const withdrawOutputScript = address
    .toOutputScript(providerAddress, network);
  const changeOutputScript = address
    .toOutputScript(providerAddress, network);

  // Create the transaction builder with the network
  const builder = new TransactionBuilder(network);

  // Estimate the transaction size and the amount of fee
  const estimatedSize = 330; // TODO do better
  const fee = feePerByte.mul(estimatedSize);

  // Compute the total amount available in all utxos to verify if no rounding
  // errors appears
  const channelTotal = utxos.reduce(
    (acc, utxo) => acc.add(utxo.amount),
    Btc.zero()
  );

  // The fee is substracted to the change amount
  const clientBalance = changeAmount.sub(fee);

  // Add in the transaction the settlement amount and the withdraw amount
  builder.addOutput(providerOutputScript, providerBalance.satoshis());
  builder.addOutput(withdrawOutputScript, withdrawAmount.satoshis());

  // Check and add change output if it's greater than zero
  if (!clientBalance.isZero()) {
    builder.addOutput(changeOutputScript, clientBalance.satoshis());
  }

  // Add all utxos in the transaction as inputs
  utxos.forEach((utxo) => {
    builder.addInput(utxo.txid, utxo.vout);
  });

  // Create an incomplete transaction because inputs are not yet signed
  const transaction = builder.buildIncomplete();

  return {
    // txid: transaction.getId(),
    tx: builder.tx.toHex(),
    // transaction: transaction,
    builder: builder,
    isComplete: false,
    estimatedSize: estimatedSize,
    utxos: getOutputs(transaction, network),
    channelState: stateTransition(
      {
        noBroadcast: clientBalance.add(withdrawAmount),
        afterBroadcast: clientBalance
      },
      {
        noBroadcast: providerBalance,
        afterBroadcast: Btc.zero()
      },
      {
        noBroadcast: channelTotal,
        afterBroadcast: channelTotal.sub(withdrawAmount).sub(providerBalance)
      },
      {
        noBroadcast: fee,
        afterBroadcast: fee
      },
      {
        noBroadcast: [0, 0],
        afterBroadcast: [0, 1]
      },
    ),
  };
};

export default withdrawTransaction;
