import { Transaction,  Network, address } from 'bitcoinjs-lib';
import { Utxo, Btc, ChannelStateTransition, Transition } from '../types';

/**
 * Return an array of UTXOs for the new created transaction. Warning: this
 * list of utxos can be false if the transaction is not complete, or can
 * change over time. This method is used to chain the transactions in the
 * channel.
 *
 * @param {Transaction} transaction the transaction object from the bitcoinjs
 * library
 * @param {Network} network the network used to compute the address
 * @return {Utxo[]} the list of all the transaction outputs on the Utxo form
 */
export const getOutputs = (
  transaction: Transaction,
  network: Network
): Utxo[] =>
  transaction.outs.map((out, index) =>
    ({
      /** the owner address of the utxo */
      address: address.fromOutputScript(out.script, network),
      /** the amout in bitcoin */
      amount: Btc.fromSatoshis(out.value),
      /** the locking script */
      scriptPubKey: out.script.toString('hex'),
      /** the transaction hash */
      txid: transaction.getId(),
      /** the output index in the txid */
      vout: index,
      /** the number of blocks mined after */
      confirmations: 0
    }));

/**
 * Validate the balance and throw an error if the channel balance is not
 * correct. Take a total value in Btc and all the different balances.
 *
 * @param {Btc} total: the total from all utxos on the multisig i
 * @param {...Btc} balances: the different balances that should add to the total
 * @return {boolean} if the balance is valid, throw an error if not equals
 */
export const validBalancesOrThrow = (total: Btc, ...balances: Btc[]) => {
  const valid =
    balances.reduce(
      (acc, balance) => acc.add(balance), Btc.zero()
    ).equals(total);
  if (!valid) {
    throw new Error(
      `Not a valid state: \
${total.bitcoin()} != \
sum(${balances.map(balance => balance.bitcoin())})`
    );
  } else { return valid; }
};

/**
 * Generate a new state object that decribe all the balances if the transaction
 * is broadcast or not.
 *
 * @param {Object} clientBalance the before and after balance for the client
 * @param {Object} providerBalance the before and after balance for the provider
 * @param {Object} channelTotal the remaining amount in the channel before and
 * after
 * @param {Object} fee the amount of fee, the same in both case
 * @param {Object} transition the transition for the state
 * @return {ChannelStateTransition} the state transition object containing the
 * new states
 */
export const stateTransition = (
  clientBalance: { noBroadcast: Btc, afterBroadcast: Btc },
  providerBalance: { noBroadcast: Btc, afterBroadcast: Btc },
  channelTotal: { noBroadcast: Btc, afterBroadcast: Btc },
  fee: { noBroadcast: Btc, afterBroadcast: Btc },
  transition: { noBroadcast: Transition, afterBroadcast: Transition }
): ChannelStateTransition => {

  Object.keys(clientBalance).forEach(
    (key) =>
      validBalancesOrThrow(
        channelTotal[key],
        clientBalance[key],
        providerBalance[key],
        fee[key]
      )
  );

  return {
    clientBalance,
    providerBalance,
    channelTotal,
    fee,
    transition
  };
};
