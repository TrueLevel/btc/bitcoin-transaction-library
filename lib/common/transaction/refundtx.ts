import { Utxo, Btc, ChannelTx } from '../types';
import { getOutputs, stateTransition } from './helper';
import { Network, TransactionBuilder, address } from 'bitcoinjs-lib';

/**
 * Create a refunding transaction that consume the funds into the multisig
 * address i and spend it to the revPubKey address i-n. The refund address can
 * have one output (for the first refund transaction) or two output (one to the
 * revPubKey address and a second to the provider address) with the correct
 * balance between them.
 *
 * @param {Utxo[]} utxos utxos from the multisig address i
 * @param {string} revPubKeyAddress the revPubKey object containing the address
 * and the redeem script
 * @param {Btc} providerBalance the amount owned by the provider
 * @param {Btc} clientBalanceBeforeFee the amount send to the channel
 * @param {Btc} feePerByte amount of satoshis per bytes, used for the fee
 * @param {string} providerAddress the provider address
 * @param {Network} network the network used
 * @return {ChannelTx} an object that contains the transaction
 */
export const refundTransaction = (
  utxos: Utxo[],
  revPubKeyAddress: string,
  providerBalance: Btc,
  clientBalanceBeforeFee: Btc,
  feePerByte: Btc,
  providerAddress: string,
  network: Network,
): ChannelTx => {

  // Estimate the transaction size to compute the amount of fee
  const estimatedTxSize = 300; // TODO do better
  const fee = feePerByte.mul(estimatedTxSize);

  // Check if the amount in satoshis is positive and greater than zero to not
  // allow fees greater than the change amount The fee is substracted to the
  // change amount in this case
  const clientBalance = clientBalanceBeforeFee.sub(fee);

  const channelTotal = utxos.reduce(
    (acc, utxo) => acc.add(utxo.amount),
    Btc.zero()
  );

  // Create a transaction builder with the given network and add all the utoxs
  // as inputs
  const builder = new TransactionBuilder(network);
  utxos.forEach((utxo, index) => {
    builder.addInput(utxo.txid, utxo.vout);
  });

  // Add the given revPubKey contract as the output of the transaction
  builder.addOutput(revPubKeyAddress, clientBalance.satoshis());

  // Add the provider output if the refund amount is greater than zero
  // we check before add the output because at the opening of the channel the
  // refund amount is zero, because the provider has no balance at this state
  if (!providerBalance.isZero()) {
    // Compute the lockScript (pubKeyScript) for the given provider address. The
    // script is used as a second output if the provider amount is greater than
    // zero.
    const providerOutputScript = address
      .toOutputScript(providerAddress, network);
    builder.addOutput(providerOutputScript, providerBalance.satoshis());
  }

  // Create an incomplete transaction because the inputs are not signed
  const transaction = builder.buildIncomplete();

  return {
    // txid: transaction.getId(),
    tx: transaction.toHex(),
    // transaction: transaction,
    builder: builder,
    isComplete: false,
    estimatedSize: estimatedTxSize,
    utxos: getOutputs(transaction, network),
    channelState: stateTransition(
      { noBroadcast: clientBalance, afterBroadcast: Btc.zero() },
      { noBroadcast: providerBalance, afterBroadcast: Btc.zero() },
      { noBroadcast: channelTotal, afterBroadcast: Btc.zero() },
      { noBroadcast: fee, afterBroadcast: Btc.zero() },
      { noBroadcast: [1, 0], afterBroadcast: [null, null] },
    ),
  };
};

export default refundTransaction;
