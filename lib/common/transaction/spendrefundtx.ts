import {
  script,
  ECPair,
  opcodes,
  address,
  Network,
  Transaction,
  TransactionBuilder,
} from 'bitcoinjs-lib';
import { Btc, Tx, UtxoAndWif } from '../types';
import { getOutputs, validBalancesOrThrow } from './helper';

/**
 * Create a transaction for spending the content in a revPubKey contract after
 * the timelock. This transaction is used by the founder of the channel to get
 * back is money from a revPubKey contract.
 *
 * @param {UtxoAndWif[]} utxosAndWifs utxos of type revPubKey contract with
 * the corresponding node able to sing the output, must be the same revPubKey
 * contract that the redeemScript argument
 * @param {string} recipientAddress the destination address of the input-s
 * @param {Buffer} redeemScript the revPubKey contract of the input-s
 * @param {number} blocksInFuture the waiting lock time to spend
 * @param {Btc} feePerByte the amount of satoshis per byte
 * @param {Network} network the network used to create the transaction
 * @return {Tx} an object that contains the transaction
 */
export const spendRefundTransaction = (
  utxosAndWifs: UtxoAndWif[],
  recipientAddress: string,
  redeemScript: Buffer,
  blocksInFuture: number,
  feePerByte: Btc,
  network: Network
): Tx => {
  // Compute the pubKeyScript for the settlement address. Used to sends funds
  // to this address
  const recipientOutputScript =
    address.toOutputScript(recipientAddress, network);

  // Default signature hash type which signs the entire transaction except any
  // signature scripts, preventing modification of the signed parts.
  // @source https://bitcoin.org/en/glossary/sighash-all
  const HASH_TYPE = Transaction.SIGHASH_ALL;

  // Create the transaction builder with the given network
  const builder = new TransactionBuilder(network);
  // Note: A transaction version number of 2 does not have the same meaning as
  // in Bitcoin, where it is associated with support for OP CHECKSEQUENCEVERIFY
  // as specified in [BIP-68]. Zcash was forked from Bitcoin v0.11.2 and does
  // not currently support BIP 68, or the related BIPs 9, 112 and 113.
  //
  // @url https://github.com/bitcoin/bips/blob/master/bip-0068.mediawiki
  builder.setVersion(2);

  // Estimate the site of the transaction and compute the amount of fee
  const estimatedSize = 600;
  const fee = feePerByte.mul(estimatedSize);

  // Add all utxos as inputs in the transaction with the given timelock in
  // the sequence argument
  utxosAndWifs.forEach((utxoAndWif) => {
    builder
      .addInput(utxoAndWif.utxo.txid, utxoAndWif.utxo.vout, blocksInFuture);
  });

  // Reduce the amount of satoshis in the utxos and get the amount to send to
  // the recepient address
  const total = utxosAndWifs.reduce(
    (acc, utxoAndWif) => acc.add(utxoAndWif.utxo.amount),
    Btc.zero()
  );
  const clientBalance = total.sub(fee);

  validBalancesOrThrow(total, clientBalance, fee);

  // Add the output to the recepient address with the send amount
  builder.addOutput(recipientOutputScript, clientBalance.satoshis());

  // Create an incomplete transaction because the inputs are not yet signed
  const transaction = builder.buildIncomplete();

  utxosAndWifs.forEach((utxoAndWif, index) => {
    // Get the hash for signature used to sign the current input
    const signatureHash = transaction.hashForSignature(
      index,
      redeemScript,
      HASH_TYPE
    );
    // Create the scriptSig for the given transaction output in the revPubKey
    // contract. The transaction is a P2SH, the script is composed of the
    // signature for the given tx output and the complete script.
    const redeemScriptSig = script.scriptHash.input.encode(
      [
        ECPair.fromWIF(utxoAndWif.wif, network).sign(signatureHash)
          .toScriptSignature(HASH_TYPE),
        opcodes.OP_TRUE
      ],
      redeemScript
    );

    // Set the scriptSig for the current input
    transaction.setInputScript(index, redeemScriptSig);
  });

  return {
    // txid: transaction.getId(),
    tx: transaction.toHex(),
    // transaction: transaction,
    builder: builder,
    isComplete: true,
    estimatedSize: estimatedSize,
    utxos: getOutputs(transaction, network),
  };
};

export default spendRefundTransaction;
