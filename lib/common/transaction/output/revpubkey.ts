import { opcodes, script } from 'bitcoinjs-lib';

/** Declare the check sequence verify op_code */
const OP_CHECKSEQUENCEVERIFY: number = opcodes.OP_NOP3;

/**
 * Generate a compiled revPubKey contract script used in the channel for locking
 * funds to a contract that protect both sides.
 *
 * ```
 * IF
 *   <senderPubkey> CHECKSIG
 *   <blocksInFuture> CHECKSEQUENCEVERIFY DROP
 * ELSE
 *   <receiverPubkey> CHECKSIGVERIFY
 *   HASH160 <revokationHash> EQUAL
 * ENDIF
 * ```
 *
 * @param {Buffer} senderPubkey the sender public key used to lock the funds
 * with the timelock
 * @param {Buffer} receiverPubkey the receiver public key used to lock the
 * funds with the revokation secret
 * @param {number} blocksInFuture the timelock number
 * @param {Buffer} revHash the revokation hash used with the receiverPubkey
 * @return {Buffer} the compiled revPubKey script
 */
export const csvRevPubKeyOutput = (
  senderPubkey: Buffer,
  receiverPubkey: Buffer,
  blocksInFuture: number,
  revHash: Buffer
): Buffer =>
  script.compile([
    opcodes.OP_IF,
    senderPubkey,
    opcodes.OP_CHECKSIG,
    script.number.encode(blocksInFuture),
    OP_CHECKSEQUENCEVERIFY,
    opcodes.OP_DROP,
    opcodes.OP_ELSE,
    receiverPubkey,
    opcodes.OP_CHECKSIGVERIFY,
    opcodes.OP_HASH160,
    revHash,
    opcodes.OP_EQUAL,
    opcodes.OP_ENDIF
  ]);

export default csvRevPubKeyOutput;
