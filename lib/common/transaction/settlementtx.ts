import { Utxo, Btc, ChannelTx } from '../types';
import { getOutputs, stateTransition } from './helper';
import { Network, TransactionBuilder, address } from 'bitcoinjs-lib';

/**
 * Create a settlement transaction that consume all the utxos given in the
 * multisig address and send the settlement amount minus the fees into the
 * settlement address and the remaining funds into the change address. The
 * change address is the next multisig address in the channel. The transaction
 * is created unsigned and is incomplete. The amount of settlement, change and
 * fee must be equal to the total of satoshis present in the utxos, if not an
 * exception is throw.
 *
 * @param {Utxo[]} utxos a list of utxos from the current multisig address
 * @param {string} providerAddress the settlement address
 * @param {Btc} providerBalanceBeforeFee the amount in satoshis
 * @param {string} channelAddress the next multisig address
 * @param {Btc} clientBalance the reamining channel amount
 * @param {Btc} feePerByte the fee per byte
 * @param {Network} network for create the right transaction
 * @return {ChannelTx} an object that contains the transaction
 */
export const settlementTransaction = (
  utxos: Utxo[],
  providerAddress: string,
  providerBalanceBeforeFee: Btc,
  channelAddress: string,
  clientBalance: Btc,
  feePerByte: Btc,
  network: Network
): ChannelTx => {
  // convert address to output script
  const providerOutputScript = address
    .toOutputScript(providerAddress, network);

  // Create a transaction builder with the given network
  const builder = new TransactionBuilder(network);

  // Estimate the transaction size to get the amount of fee
  const estimatedSize = 300; // TODO do better
  const fee = feePerByte.mul(estimatedSize);

  // Reduce the given utxos to retreive the total amount in satoshis
  const channelTotal = utxos.reduce(
    (acc, utxo) => acc.add(utxo.amount),
    Btc.zero()
  );

  // The fee is inputed to the provider because he decided to broadcast the
  // transaction
  const providerBalance = providerBalanceBeforeFee.sub(fee);

  // Add the settlement amount output in the transaction and the change amount
  // output
  builder.addOutput(providerOutputScript, providerBalance.satoshis());
  builder.addOutput(channelAddress, clientBalance.satoshis());

  // Add all inputs in the transaction
  utxos.forEach((utxo) => {
    builder.addInput(utxo.txid, utxo.vout);
  });

  // Create an incomplete transaction because the inputs are not yet signed
  const transaction = builder.buildIncomplete();

  return {
    // txid: transaction.getId(),
    tx: transaction.toHex(),
    // transaction: transaction,
    builder,
    isComplete: false,
    estimatedSize,
    utxos: getOutputs(transaction, network),
    channelState: stateTransition(
      {
        noBroadcast: clientBalance,
        afterBroadcast: clientBalance
      },
      {
        noBroadcast: providerBalance,
        afterBroadcast: Btc.zero()
      },
      {
        noBroadcast: channelTotal,
        afterBroadcast:
        channelTotal.sub(providerBalance)
      },
      {
        noBroadcast: fee,
        afterBroadcast: fee
      },
      {
        noBroadcast: [1, 0],
        afterBroadcast: [0, 1]
      },
    ),
  };
};

export default settlementTransaction;
