import { Btc, ChannelTx, UtxoAndWif } from '../types';
import { getOutputs, stateTransition } from './helper';
import { Network, TransactionBuilder, ECPair } from 'bitcoinjs-lib';

/**
  * Create a funding transaction that consume all the given utxos
  * and send it to the channel address. Each input must be signed
  * by the funding address node.
  *
  * @param {UtxoAndWif[]} utxosAndWifs object containing and array with
  * properties utxo and the corresponding WIF that unlocks the respective utxo,
  * used to fund the channel
  * @param {string} channelAddress the output script for the channel address
  * @param {Btc} feePerByte amount of satoshis per bytes of fee
  * @param {Network} network the network used to create the tx
  * @return {ChannelTx} an object that contains the transaction
  */
export const fundingTransaction = (
  utxosAndWifs: UtxoAndWif[],
  channelAddress: string,
  feePerByte: Btc,
  network: Network
): ChannelTx => {

  // Create the transaction builder and add all utxos as inputs
  const builder = new TransactionBuilder(network);
  utxosAndWifs.forEach((utxoAndWif) =>
    builder.addInput(utxoAndWif.utxo.txid, utxoAndWif.utxo.vout));

  // Count the total satoshis available by reducing the amount of utxos
  const channelTotal = utxosAndWifs.reduce(
    (acc, utxoAndWif) => acc.add(utxoAndWif.utxo.amount),
    Btc.zero()
  );

  // Estimate the fee and the final amount
  const estimatedSize = utxosAndWifs.length * 180 + 34 + 10; // TODO do better
  const fee = feePerByte.mul(estimatedSize);
  const clientBalance = channelTotal.sub(fee);

  // Add only one output to the given channel address and sign all the registred
  // inputs in the transaction builder
  builder.addOutput(channelAddress, clientBalance.satoshis());
  utxosAndWifs.forEach(
    (utxoAndWif, index) =>
      builder.sign(
        index,
        ECPair.fromWIF(utxoAndWif.wif, network)
      )
  );

  // Generate the complete transaction
  const transaction = builder.build();

  return {
    // txid: transaction.getId(),
    tx: transaction.toHex(),
    // transaction: transaction,
    builder: builder,
    isComplete: true,
    estimatedSize: estimatedSize,
    utxos: getOutputs(transaction, network),
    channelState: stateTransition(
      { noBroadcast: Btc.zero(), afterBroadcast: clientBalance },
      { noBroadcast: Btc.zero(), afterBroadcast: Btc.zero() },
      { noBroadcast: Btc.zero(), afterBroadcast: channelTotal },
      { noBroadcast: Btc.zero(), afterBroadcast: fee },
      { noBroadcast: [null, null], afterBroadcast: [0, 0] },
    ),
  };
};

export default fundingTransaction;
