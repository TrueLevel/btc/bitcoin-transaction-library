import { BigNumber } from 'bignumber.js';

const BigInteger = BigNumber.another(
  {
    DECIMAL_PLACES: 0,
    // HALF_FLOOR 8 Towards nearest neighbour. If equidistant, towards
    // -Infinity.
    ROUNDING_MODE: 8,
    RANGE: [-15, 15]
  });


/**
 * The Btc class is an helper to deal with the bitcoin unit and the satoshis
 * unit. Btc provide static methods to create a Btc object from bitcoin unit
 * and satoshis unit and provides methods to get the value in bitcoin and
 * satoshis.
 */
export class Btc {

  /** constant used to transform satoshi to bitcoin and bitcoin to satoshi */
  private static _SATOSHI_CONSTANT = 1e8;

  /** constant max number of bitcoins in existence */
  private static _SATOSHI_MAX = 2.1e15;

  /** validade whether the value can be converted to satoshis */
  public static isValidSatoshi = (_v: number | BigNumber): boolean => {
    const value = typeof _v === 'number' ? _v : _v.toNumber();
    return value >= 0
      && Btc._SATOSHI_MAX >= value
      && Math.floor(value) === value;
  }
  /** store the value in satoshis */
  private _value: BigNumber;

  /** construct an object an assign the given value in satoshis */
  private constructor(_v: number | BigNumber) {
    if (Btc.isValidSatoshi(_v)) {
      this._value = new BigInteger(_v);
    } else {
      throw new Error(`Value ${_v.valueOf()} cannot be in Satoshis`);
    }
  }

  /**
   * Return a Btc object after checking the validity of the given satoshis
   * value.
   *
   * @throw {Error} if the value is a floating number (not possible in satoshis)
   * @throw {Error} if the value is lower or equal to zero
   * @throw {Error} if the value exceed the number of total bitcoin available
   * @param {number} value the number of satoshis
   * @return {Btc} a Btc object with the given value
   */
  public static fromSatoshis = (value: number | BigNumber) =>
    new Btc(value);

  /**
   * Return a Btc object after checking the validity of the given bitcoin
   * value.
   *
   * @param {number} value the number of bitcoin
   * @return {Btc} a Btc object with the given value
   */
  public static fromBitcoin = (value: number) =>
    Btc.fromSatoshis(
      new BigInteger(value)
        .times(Btc._SATOSHI_CONSTANT)
    )

  /**
   * Return a zero value in btc.
   *
   * @return {Btc} with a zero value
   */
  public static zero = () => Btc.fromSatoshis(0);

  /**
   * Return the value cast in bitcoin.
   *
   * @return {number} the value in btc
   */
  public bitcoin = (): number =>
    new BigNumber(this.satoshis())
      .div(Btc._SATOSHI_CONSTANT)
      .toNumber()

  /**
   * Return the value cast in satoshis.
   *
   * @return {number} the value in satoshis
   */
  public satoshis = (): number =>
    this._value.toNumber()

  /**
   * Return a new Btc object that contains the result of the additnew Btc(0);
   ￼
   ion.
   *
   * @param {Btc} a the bitcoin to add
   * @return {Btc} the result of the addition
   */
  public add = (a: Btc): Btc =>
    Btc.fromSatoshis(
      this._value
        .add(a._value)
    )

  /**
   * Return a new Btc object that contains the result of the substration.
   *
   * @param {Btc} a the bitcoin to substract
   * @return {Btc} the result of the substration
   */
  public sub = (a: Btc): Btc =>
    Btc.fromSatoshis(
      this._value
        .sub(a._value)
    )

  /**
   * Return a new Btc object that contains the result of the multiplication.
   *
   * @param {number | Btc} a the multiplier
   * @return {Btc} the result of the multiplication
   */
  public mul = (a: Btc | number): Btc =>
    Btc.fromSatoshis(
      this._value
        .mul(typeof a === 'number' ? a : a._value)
    )

  /**
   * Return a new Btc object that contains the result of the division.
   *
   * @param {number | Btc} a the divisor
   * @return {Btc} the result of the division
   */
  public div = (a: Btc | number): Btc =>
    Btc.fromSatoshis(
      this._value
        .div(typeof a === 'number' ? a : a._value)
    )

  /**
   * Return true if the two object have equal values, false otherwise.
   *
   * @param {Btc} a the bitcoin to test with
   * @return {boolean} the equality
   */
  public equals = (a: Btc): boolean =>
    this._value.equals(a._value);

  /**
   * Return true if the value is zero, false otherwise.
   *
   * @return {boolean} true if zero, false otherwise
   */
  public isZero = (): boolean =>
    this._value.equals(0)

  /**
   * Return true if the current object is less than a, false otherwise.
   *
   * @param {Btc} a the bitcoin to test with
   * @return {boolean}
   */
  public lessThan = (a: Btc): boolean =>
    this._value.lessThan(a._value);

  /**
   * Return true if the current object is less than or equal to a, false
   * otherwise.
   *
   * @param {Btc} a the bitcoin to test with
   * @return {boolean}
   */
  public lessThanOrEqual = (a: Btc): boolean =>
    this._value.lessThanOrEqualTo(a._value);

  /**
   * Return true if the current object is greater than a, false otherwise.
   *
   * @param {Btc} a the bitcoin to test with
   * @return {boolean}
   */
  public greaterThan = (a: Btc): boolean =>
    this._value.greaterThan(a._value);

  /**
   * Return true if the current object is greater than or equal to a, false
   * otherwise.
   *
   * @param {Btc} a the bitcoin to test with
   * @return {boolean}
   */
  public greaterThanOrEqual = (a: Btc): boolean =>
    this._value.greaterThanOrEqualTo(a._value);

}
