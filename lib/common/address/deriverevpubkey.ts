import { ContractType, ClientProviderPubkeys } from '../types';
import { Network, crypto, script, address } from 'bitcoinjs-lib';
import { csvRevPubKeyOutput } from '../transaction/output/revpubkey';

/**
 * Create the custom P2SH called revPubKey contract in the paper. This P2SH
 * contains two branch: (i) a revokation hash and a signature, (ii) a timelock
 * and a signature. The client has to wait for the timelock before spending the
 * money and the provider can take the money if he knows the revokation hash.
 *
 * @param {MultisigPubkeys} pubkeys all the keys used in the multisig address
 * @param {Buffer} revHash the revokation hash used in the contract
 * @param {number} blocksInFuture the number of block for the timelock
 * @param {Network} network the network used to create the contract address
 * @return {ContractType} the RevPubKey contract
 */
export const deriveRevPubkey = (
  pubkeys: ClientProviderPubkeys,
  revHash: Buffer,
  blocksInFuture: number,
  network: Network
): ContractType & { revHash: Buffer } => {
  // Generate the contract script with the keys, the timelock and the revokation
  // secret hash
  const redeemScript = csvRevPubKeyOutput(
    pubkeys.clientPubkey,
    pubkeys.providerPubkey,
    blocksInFuture,
    revHash
  );

  // Create the pay-to-script-hash script used in the transaction and get the
  // address of the script
  const scriptPubKey = script.scriptHash.output.encode(
    crypto.hash160(redeemScript)
  );
  const revPubKeyAddress = address.fromOutputScript(scriptPubKey, network);

  return {
    address: revPubKeyAddress,
    redeemScript,
    revHash,
  };
};

export default deriveRevPubkey;
