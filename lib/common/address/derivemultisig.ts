import { ContractType, ClientProviderPubkeys } from '../types';
import { Network, crypto, script, address } from 'bitcoinjs-lib';

/**
 * Create the multisig address and the corresponding redeemScript for the
 * given state i. The MultisigPubkeys object contains both HDNode derived to
 * obtain the nodes to create the multi signature script for the given network.
 *
 * @param {MultisigPubkeys} pubkeys all the keys used in the multisig address
 * @param {Network} network the network used to derive the multisig
 * @return {ContractType} the ChannelAddress object
 */
export const deriveMultisig = (
  pubkeys: ClientProviderPubkeys,
  network: Network
): ContractType => {
  const redeemScript = script
    .multisig.output.encode(2, [pubkeys.clientPubkey, pubkeys.providerPubkey]);
  const scriptMultisig = script.scriptHash.output.encode(
    crypto.hash160(redeemScript)
  );
  const channelAddress = address
    .fromOutputScript(scriptMultisig, network);
  return {
    address: channelAddress,
    redeemScript,
  };
};

export default deriveMultisig;
