import { Btc } from './btc';
import { TransactionBuilder } from 'bitcoinjs-lib';

import {
  CreateFundingTransaction,
  InitiateTransaction,
} from '../protocol/Client';

import {
  CreateSettlementTransaction,
} from '../protocol/Server';

// Re-exports
export {
  Btc,
  CreateFundingTransaction,
  InitiateTransaction,
  CreateSettlementTransaction,
  TransactionBuilder,
};

/**
 * An utxo represent an unspend transaction output. This structure can be used
 * to type insight, rpc and common output utxos.
 */
export interface Utxo {
  /** the owner address of the utxo */
  address: string;
  /** the amout in bitcoin */
  amount: Btc;
  /** the locking script */
  scriptPubKey: string;
  /** the transaction hash */
  txid: string;
  /** the output index in the txid */
  vout: number;
  /** the number of blocks mined after */
  confirmations: number;
  /** the amount in satoshis */
  satoshis?: number;
  /** the block height */
  height?: number;
  /** the account */
  account?: string;
  /** if the utox is spendable or not */
  spendable?: boolean;
  /** solvable */
  solvable?: boolean;
}

/**
 * Represent and Utxo and the private key in WIF format, the key should be able
 * to sign the unspend output.
 */
export interface UtxoAndWif {
  /** an unspend output */
  utxo: Utxo;
  /** the corresponding private key able to sign in WIF format */
  wif: string;
}

/**
 * Represent a pair of public keys. These public key are used to create a mutli-
 * signature script, this script can be converted into an address with a pay-to-
 * script-hash.
 */
export interface ClientProviderPubkeys {
  /** the client public key in the right derivation path */
  clientPubkey: Buffer;
  /** the server public key in the right derivation path */
  providerPubkey: Buffer;
}

export type User = 'server' | 'client';

/**
 * Represent the apis supported by the library for network communication.
 *
 * has to be explicitly specified as tuple (otherwise inferred as array of
 * strings) for type inference to work, the reason of repeating.
 * ref stackoverflow: https://goo.gl/Bh8wn6
 */
export const apiName: ['btcrpc', 'insight'] = ['btcrpc', 'insight'];
export type ApiName = typeof apiName[number];

/**
 * Represent the three different network in bitcoin.
 *
 * has to be explicitly specified as tuple (otherwise inferred as array of
 * strings) for type inference to work, the reason of repeating.
 * ref stackoverflow: https://goo.gl/Bh8wn6
 */
export const networkName: ['bitcoin', 'regtest', 'testnet'] =
  ['bitcoin', 'regtest', 'testnet'];
export type NetworkName = typeof networkName[number];



/**
 * Used to normalize the interface to interact with different bitcoin API like
 * web or RPC.
 */
export interface RequestApi {
  /**
   * Get the list of all unspend output for the given address
   *
   * @param {string} address the address used to search the utxos
   * @return {Promise} a promise that contains the utxos
   */
  getUtxos(address: string): Promise<Utxo[]>;

  /**
   * Get an estimate fee for a given block target
   *
   * @param {number} blockTarget the target block height
   * @return {Promise} a promise that contains the estimate
   */
  getEstimateFee(blockTarget: number): Promise<Btc>;

  /**
   * Broadcast a transaction and get the result of this action
   *
   * @param {string} txhex the raw transaction in hex value
   * @return {Promise} a promise that contains broadcast result
   */
  broadcastTx(txhex: string): Promise<string>;
}

/**
 * Represent a RevPubKey or a 2 of 2 Multisig contract object. Used by
 * transactions to interact with the custom locking scripts.
 */
export interface ContractType {
  /** the address used for a P2SH transaction */
  address: string;
  /** the complete script */
  redeemScript: Buffer;
}

/**
 * Represent a generated transaction output by the helper functions. Contains
 * the transaction with the builder and the complete balance of the channel
 * including the applied amount of fee in the transaction and the estimated
 * size.
 */
export interface Tx {
  // /** the id of the transaction */
  // txid: string;
  /** the hex representation of the transaction */
  tx: string;
  // /** the transaction object from the bitcoinjs library */
  // transaction: Transaction;
  /** the transaction builder object from the bitcoinjs library */
  builder: TransactionBuilder;
  /** is the transaction complete and ready to be broadcasted?  */
  isComplete: boolean;
  /** the estimated size used to compute the fee */
  estimatedSize: number;
  /** the outputs of the current transactions */
  utxos: Utxo[];
}

/**
 * Represent all the possible transaction for the state i and n.
 */
export type Transition = [0, 0] | [0, 1] | [1, 0] | [null, null];

/**
 * The channel state transition represent all the possible balances for a given
 * transaction if the transaction is broadcasted or not.
 */
export interface ChannelStateTransition {
  /** the amount due to the client [ currently, afterBroadcast ] */
  clientBalance: { noBroadcast: Btc; afterBroadcast: Btc };
  /** the amount due to the provider [ currently, afterBroadcast ] */
  providerBalance: { noBroadcast: Btc; afterBroadcast: Btc };
  /** the total value locked in the channel [ currently, afterBroadcast ] */
  channelTotal: { noBroadcast: Btc; afterBroadcast: Btc };
  /** the state transition [n, i], [null, null] closes the channel */
  transition: { noBroadcast: Transition; afterBroadcast: Transition };
  /** the fee value included in the transaction */
  fee: { noBroadcast: Btc; afterBroadcast: Btc };
}

/**
 * Export the channel tx type that extend the tx type with a channel state
 * transition object.
 */
export type ChannelTx = Tx & { channelState: ChannelStateTransition };
