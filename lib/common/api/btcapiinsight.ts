import { get, post } from 'request-promise';
import { Utxo, NetworkName, RequestApi, Btc } from '../types';

export const BTCApiInsight = (net: NetworkName = 'testnet'): RequestApi => {

  const url = () => {
    switch (net) {
      case 'bitcoin':
        return `https://insight.bitpay.com`;
      case 'testnet':
        return `https://test-insight.bitpay.com`;
      default:
        throw new Error(
          `Adapter: Insight is not allowed in combination with Network: ${net}`
        );
    }
  };

  // async/await was used below to make the api output types homogeneous at the
  // level of btcapi.ts class. Wrap one native Promise monad onto an unwrapped
  // bluebird promise monad
  return {
    getUtxos: async (address: string): Promise<Utxo[]> => {
      const utxos: Array<Utxo & { amount: number }> =
        JSON.parse(await get(`${url()}/api/addrs/${address}/utxo`));
      return utxos.map((utxo) =>
        ({ ...utxo, amount: Btc.fromBitcoin(utxo.amount) }));
    },

    // blockTarget value is an attribute of the received object that contains
    // the fee
    getEstimateFee: async (blockTarget: number): Promise<Btc> =>
      Btc.fromBitcoin(
        JSON.parse(
          await get(
            `${url()}/api/utils/estimatefee?nbBlocks=${blockTarget.toString()}`
          )
        )[blockTarget]
      ),

    broadcastTx: async (tx: string): Promise<string> =>
      await post({
        url: `${url()}/api/tx/send/`,
        method: 'POST',
        json: { rawtx: tx }
      })
  };
};

export default BTCApiInsight;
