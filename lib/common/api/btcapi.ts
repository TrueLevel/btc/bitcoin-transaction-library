import { BTCApiRPC } from './btcapirpc';
import { BTCApiInsight } from './btcapiinsight';
import { NetworkName, RequestApi, ApiName } from '../types';

export const BTCApi = (
  net: NetworkName = 'testnet',
  apiName: ApiName = 'btcrpc'
): RequestApi => {
  switch (apiName) {
    case 'btcrpc':
      return BTCApiRPC(net);
    case 'insight':
      return BTCApiInsight(net);
  }
};

export default BTCApi;
