import { Btc } from '../btc';
import { get } from 'request-promise';

export const getFeePerByte =
  async (priority: 'fastestFee' | 'halfHourFee' | 'hourFee'): Promise<Btc> => {
    const url = `https://bitcoinfees.21.co/api/v1/fees/recommended`;
    const allFees = JSON.parse(await get(url));
    return Btc.fromSatoshis(allFees[priority]);
  };
