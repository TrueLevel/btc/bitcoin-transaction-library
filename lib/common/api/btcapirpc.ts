import * as RpcClient from 'bitcoin-core';
import { Utxo, RequestApi, NetworkName, Btc } from '../types';

const host = process.env.RPC_HOST || '127.0.0.1';
const common = {
  // network: net,
  host: host,
  username: 'test',
  password: 'test'
};

export const BTCApiRPC = (net: NetworkName = 'testnet'): RequestApi => {

  const client = (): RpcClient => {
    switch (net) {
      case 'bitcoin':
        return new RpcClient({ port: 8332, ...common });
      case 'testnet':
        return new RpcClient({ port: 18332, ...common });
      case 'regtest':
        return new RpcClient({ port: 18332, ...common });
    }
  };

  return {
    getUtxos: async (address: string): Promise<Utxo[]> => {
      const utxos: Array<Utxo & { amount: number }> =
        await client().listUnspent(0, 9999999, [address]);
      return utxos.map((utxo) =>
        ({ ...utxo, amount: Btc.fromBitcoin(utxo.amount) }));
    },

    getEstimateFee: async (blockTarget: number): Promise<Btc> =>
      Btc.fromBitcoin(await client().estimateFee(blockTarget)),

    broadcastTx: async (tx: string): Promise<string> =>
      await client().sendRawTransaction(tx)
  };
};

export default BTCApiRPC;
