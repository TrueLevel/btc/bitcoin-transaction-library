import { randomBytes } from 'crypto';
import { HDNode, Network } from 'bitcoinjs-lib';

/**
 * Create a random HDNode and return his representation in base58. This method
 * is used to generate random nodes in the demo.
 *
 * @param {Network} network the network used to generate the Node
 * @return {string} the base58 representation
 */
export const generateSeed = (network: Network): string =>
  HDNode.fromSeedBuffer(randomBytes(32), network).toBase58();

export default generateSeed;
