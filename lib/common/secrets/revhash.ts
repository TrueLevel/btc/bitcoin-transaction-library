import { crypto } from 'bitcoinjs-lib';

/**
 * Create the revokation hash (hash160 = RIPEMD160(SHA256())) from the secret.
 *
 * @param {Buffer} revSecret the revocation secret
 * @return {Buffer} the hash160 of the secret
 */
export const revHashFromSecret = (revSecret: Buffer): Buffer => {
  if (revSecret.length > 0) {
    return crypto.hash160(revSecret);
  } else {
    throw new Error('The revSecret must be a non-empty buffer!');
  }
};

export default revHashFromSecret;
