import { HDNode, crypto } from 'bitcoinjs-lib';
import { derivation } from '../node/derivation';
import { I, N } from '../../state/state';

/**
 * Derive the secret with the client node, used for revocation purpose.
 *
 * @param {HDNode} clientAccount the root node used to derive the secret
 * @param {I} i the index i used to derive the second part of the secret
 * @param {N} n the index n used to derive the first part of the secret
 * @return {Buffer} the secret for the given channel state
 */
export const deriveRevSecret = (
  clientAccount: HDNode,
  i: I,
  n: N
): Buffer => {
  // Derive the hardened secret with the parameter n
  const muN: Buffer =
    derivation.secret(clientAccount, n).keyPair.d.toBuffer(32);
  // Derive the private revPubkey contract key with the parameter i
  const gammaI: Buffer =
    derivation.revPubkey(clientAccount, i).keyPair.d.toBuffer(32);

  const protoSecret = Buffer.concat(
    [muN, gammaI],
    muN.length + gammaI.length
  );

  // compute hash( muN || gammaI )
  const secret = crypto.sha256(protoSecret); // TODO(Joel): hash160

  return secret;
};

export default deriveRevSecret;
