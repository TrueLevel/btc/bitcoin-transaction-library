import {
  HDNode,
  TransactionBuilder,
  crypto,
  ECSignature,
} from 'bitcoinjs-lib';
import { ClientProviderPubkeys, User } from '../types';

export const verifySignature = (
  builder: TransactionBuilder,
  clientNode: HDNode,
  providerNode: HDNode,
  pubkeys: ClientProviderPubkeys,
  redeemScript: Buffer,
  signatureToBeVerified: User,
) => {

  let nodeToVerify: HDNode;
  let indexToVerify: number;
  switch (signatureToBeVerified) {
    case 'client':
      nodeToVerify = clientNode;
      indexToVerify = 0;
      break;
    case 'server':
      nodeToVerify = providerNode;
      indexToVerify = 1;
      break;
    default:
      throw new Error(`Unknown party ${signatureToBeVerified}`);
  }

  const verifiedScript =
    builder.inputs[0].prevOutScript.slice(2, -1)
      .equals(crypto.hash160(redeemScript));

  const ecsig = ECSignature
    .parseScriptSignature(builder.inputs[0].signatures[indexToVerify]);

  const hash = builder.tx.hashForSignature(0, redeemScript, ecsig.hashType);

  const verifiedSignature =
    nodeToVerify
      .verify(hash, ecsig.signature);

  return { verifiedScript, verifiedSignature };
};
