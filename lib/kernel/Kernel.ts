import {
  ApiName,
  apiName,
  RequestApi,
  NetworkName,
  networkName,
} from '../common/types';
import { BTCApi } from '../common/api/btcapi';
import { HDNode, Network, networks } from 'bitcoinjs-lib';
import { Multisig, RevPubkey } from './Contract';
import { derivation } from '../common/node/derivation';

/**
 * This class represent an abstract channel with all the methods used to
 * derive channel addresses, get locktime or get public keys for a given state.
 * This class is extends to create a specialized implementation for the client
 * and the server.
 */
export class Kernel {

  //////////////////////////////////////////////////////////////////////////////
  // PROTECTED PROPERTIES
  //////////////////////////////////////////////////////////////////////////////

  /** The network object is a private reference used to create the transaction
   * for the right network. The network is set at the construction of the object
   * an cannot be change after. */
  protected _network: Network;

  /** The private request api object point to the choosen api define at the
   * construction of the object. It cannot be change after. */
  protected _adapter: RequestApi;

  /** The account id is a private integer greater than 3 used in the derivation
   * path of the HDNode to generate the keys used in the channel. The account
   * id is also the channel id between the client and the server. */
  protected _accountId: number;

  /** The client public account stores the client node for the same path
   * to derive the client public's key. */
  protected _clientAccPub: HDNode;

  /** The server public account stores the server node for the same path
   * to derive the server public's key. */
  protected _serverAccPub: HDNode;

  protected _accPriv: HDNode;

  protected _fundingRoot?: HDNode;
  //////////////////////////////////////////////////////////////////////////////
  // PUBLIC PROPERTIES
  //////////////////////////////////////////////////////////////////////////////


  public revPubkey: RevPubkey;
  public multisig: Multisig;

  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTOR
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Create channel used to interact with the client and server.
   *
   * @throw {Error} wrong network identifier given
   * @throw {Error} an account id lesser than 4
   * @param {string} clientKey the client root key in base58
   * @param {string} serverKey the server public key for the
   * same path m/44'/0'/[accountId]'
   * @param {number} accountId the channel id between the client and the
   * server
   * @param {NetworkName} net the network identifier
   * @param {ApiName} api the api used to query the network
   * @param {number} _blocksInFuture the locktime used for the channel
   * @return {ClientChannel} the channel used to interact with the server
   */
  constructor(
    clientKey: string,
    serverKey: string,
    accountId: number,
    net: NetworkName = 'testnet',
    api: ApiName = 'btcrpc',
    protected _blocksInFuture: number = 10
  ) {

    if (networkName.every((network) => network !== net)) {
      throw new Error(`Unsupported network ${net}`);
    }

    if (apiName.every((a) => a !== api)) {
      throw new Error(`Unsupported api ${api}`);
    }

    // Set the network and create the adapter with the given network
    this._network =
      networks[net === 'regtest'
        ? 'testnet'
        : net
      ];

    this._adapter = BTCApi(net, api);

    // AccountId must be greater than 3 (0 is reserved for secret, 1 is reserved
    // for funding/deposit addresses, 2 is reserved for refund addresses)
    if (accountId > 3) {
      this._accountId = accountId;
    } else {
      throw new Error('The account id must be greater than 3!');
    }

    // Determine what user has access to private key
    const clientKeyNode = HDNode.fromBase58(clientKey, this._network);
    const clientNeutered = clientKeyNode.isNeutered();

    const serverKeyNode = HDNode.fromBase58(serverKey, this._network);
    const serverNeutered = serverKeyNode.isNeutered();

    if (!clientNeutered && !serverNeutered) {
      throw new Error('Both private keys are known!');
    // } else if (clientNeutered && serverNeutered) {
    //   throw new Error('None of the private keys are know!');
    }

    if (clientNeutered) {
      this._clientAccPub = clientKeyNode;
    } else {
      this._accPriv =
        derivation.privateAccount(clientKeyNode, this._accountId);
      this._clientAccPub = this._accPriv.neutered();
      this._fundingRoot = derivation.funding(this._accPriv);
    }

    if (serverNeutered) {
      this._serverAccPub = serverKeyNode;
    } else {
      this._accPriv =
        derivation.privateAccount(serverKeyNode, this._accountId);
      this._serverAccPub = this._accPriv.neutered();
    }

    if (this._clientAccPub.getAddress() === this._serverAccPub.getAddress()) {
      throw new Error('Client and Server have the same private key');
    }
    // instanciate the Contract objects used to build contracts
    this.revPubkey = new RevPubkey(
      this._clientAccPub,
      this._serverAccPub,
      this._network,
      this._blocksInFuture,
      this._accPriv,
    );

    this.multisig = new Multisig(
      this._clientAccPub,
      this._serverAccPub,
      this._network,
      this._blocksInFuture,
      this._accPriv,
    );

  }

  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE FUNCTIONS
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Return the value of blocksInFuture.
   *
   * @return {number}
   */
  public getBlocksInFuture = (): number => this._blocksInFuture;

  /**
   * Return the configured Network.
   *
   * @return {Network}
   */
  public getNetwork = (): Network => this._network;

  /**
   * Return the configured adapter. Used by the other library to get utxos and
   * fees.
   *
   * @return {RequestApi} the request api proxy already configured
   */
  public getAdapter = (): RequestApi => this._adapter;

  /**
   * Set the locktime blocksInFuture with a new number.
   *
   * @param {number} blocksInFuture the new value for the locktime
   * @return {null}
   */
  public setBlocksInFuture = (blocksInFuture: number) => {
    this._blocksInFuture = blocksInFuture;
  }


  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE FUNCTIONS
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Generate an address controlled by the client. Used in the demo to get
   * funds in a controlled address before funding the multisig address.
   *
   * @param {number} i the index of the multisig address
   * @return {HDNode} the derived node
   */
  public deriveFundingNode = (j: number): HDNode => {
    if (this._fundingRoot) {
      return this._fundingRoot.derive(j);
    } else {
      throw new Error(
        `Cannot derive funding node without Client's private key`
      );
    }
  }
}
