import {
  ContractType,
  ClientProviderPubkeys,
  User,
} from '../common/types';
import { derivation } from '../common/node/derivation';
import {
  HDNode,
  Network,
  TransactionBuilder,
} from 'bitcoinjs-lib';
import { deriveRevPubkey } from '../common/address/deriverevpubkey';
import { deriveMultisig } from '../common/address/derivemultisig';
import { I, N } from '../state/state';
import { deriveRevSecret } from '../common/secrets/revsecret';
import { verifySignature } from '../common/signature/verifysig';
import { revHashFromSecret } from '../common/secrets/revhash';

abstract class Contract {
  protected abstract _contractType: 'multisig' | 'revPubkey';
  protected abstract _clientAccPub: HDNode;
  protected abstract _serverAccPub: HDNode;
  protected abstract _accPriv?: HDNode;
  public abstract deriveContract(i: I, nOrHash?: N | Buffer): ContractType;

  public getClientNode = (i: I) =>
    derivation[this._contractType](this._clientAccPub, i);

  public getProviderNode = (i: I) =>
    derivation[this._contractType](this._serverAccPub, i);

  public getKeyPair = (i: I) => {
    if (this._accPriv) {
      return derivation[this._contractType](this._accPriv, i).keyPair;
    } else {
      throw new Error('Cannot derive Revocation secret without private key');
    }
  }
  public getPubkeys = (i: I): ClientProviderPubkeys =>
    ({
      clientPubkey: this.getClientNode(i).getPublicKeyBuffer(),
      providerPubkey: this.getProviderNode(i).getPublicKeyBuffer(),
    })

}

export class Multisig extends Contract {
  protected _contractType: 'multisig' = 'multisig';
  constructor(
    protected _clientAccPub: HDNode,
    protected _serverAccPub: HDNode,
    protected _network: Network,
    protected _blocksInFuture: number,
    protected _accPriv: HDNode,
  ) { super(); }


  /**
   * Generate the multisig address for the given index i in the white paper.
   * The multisig address is used to lock the funds and open the channel.
   *
   * @param {I} i the index i of the multisig address
   * @return {ContractType} the channel address object with the contract
   */
  public deriveContract(i: I) {
    return deriveMultisig(
      this.getPubkeys(i),
      this._network
    );
  }

  public verifySignature = (
    builder: TransactionBuilder,
    i: I,
    signatureToVerify: User,
  ) =>
    verifySignature(
      builder,
      this.getClientNode(i),
      this.getProviderNode(i),
      this.getPubkeys(i),
      this.deriveContract(i).redeemScript,
      signatureToVerify,
    );
}

export class RevPubkey extends Contract {
  protected _contractType: 'revPubkey' = 'revPubkey';
  constructor(
    protected _clientAccPub: HDNode,
    protected _serverAccPub: HDNode,
    protected _network: Network,
    protected _blocksInFuture: number,
    protected _accPriv: HDNode,
  ) { super(); }


  /**
   * Create the custom p2sh called revpubkey contract in the paper. This p2sh
   * contains two branch: (i) a revokation hash and a signature, (ii) a timelock
   * and a signature. the client has to wait for the timelock before spending
   * the money and the provider can take the money if he knows the revokation
   * hash.
   *
   * @param {I} i - the index i used to create the revPubKey contract
   * @param {N | Buffer} revokationHash - the revokation hash used in the
   * contract or the index N
   */
  public deriveContract(i: I, nOrRevHash: N | Buffer) {
    return deriveRevPubkey(
      this.getPubkeys(i),
      Buffer.isBuffer(nOrRevHash)
        ? nOrRevHash
        : revHashFromSecret(this.deriveRevSecret(i, nOrRevHash)),
      this._blocksInFuture,
      this._network,
    );
  }

  /**
   * Get the secret for a state i, n.
   *
   * @param {I} i the secret index i
   * @param {N} n the secret index n
   * @return {string} the secret
   */
  public deriveRevSecret = (i: I, n: N) => {
    if (this._accPriv) {
      return deriveRevSecret(this._accPriv, i, n);
    } else {
      throw new Error('Cannot derive Revocation secret without private key');
    }
  }

  public verifySignature = (
    builder: TransactionBuilder,
    i: I,
    n: N,
    signatureToVerify: User,
  ) =>
    verifySignature(
      builder,
      this.getClientNode(i),
      this.getProviderNode(i),
      this.getPubkeys(i),
      this.deriveContract(i, n).redeemScript,
      signatureToVerify,
    );
}
