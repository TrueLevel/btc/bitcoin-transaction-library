import {
  client,
  clientKernel,
  server,
  serverKernel,
  adapter,
  rpc,
  // blocksInFuture,
  feePerByte,
  InitiateTransaction,
  CreateFundingTransaction,
  CreateSettlementTransaction,
  TransactionBuilder,
} from './environment';
import { test } from 'ava';
import { Btc, Utxo } from '../lib/common/types';
import { createInitialTransactions } from './helper';

import { State } from '../lib/state/state';

let state: State;
let utxosMultisig: Utxo[];
let multisigAddress: string;
let f: CreateFundingTransaction;
let newClientTxs: InitiateTransaction;
let newProviderTxs: CreateSettlementTransaction;
let newRefundtxProviderClient: TransactionBuilder;
let refundtxProvider: string;

test.beforeEach('fund client controlled address and go to i+1', async (t) => {
  let clientBalance: Btc;
  let providerBalance: Btc;
  [
    state,
    utxosMultisig,
    multisigAddress,
    f,
    newClientTxs,
    newProviderTxs,
    newRefundtxProviderClient,
    refundtxProvider,
    clientBalance,
    providerBalance
  ] = await createInitialTransactions(
    t, // we pass t because some tests are performed in this function
    client,
    clientKernel,
    server,
    serverKernel,
    rpc,
    adapter,
    feePerByte
  );

 // go tddo next n,
  state = state.next.n();

  const transferAmount = Btc.fromBitcoin(0.002);
  providerBalance = providerBalance.add(transferAmount);
  clientBalance = clientBalance.sub(transferAmount);

  // cant do next line because of fees ?
  // TODO: get exact balance after fees
  // t.is(clientBalance.bitcoin(), 0.095);
  t.is(providerBalance.bitcoin(), 0.005);


  // should get previous secret:
  // const prevSecret = newClientTxs.previousSecret;

  newClientTxs = client.initiateTransaction(
    utxosMultisig,
    state.i,
    state.n,
    providerBalance,
    clientBalance,
    feePerByte,
    clientKernel.multisig.getProviderNode(state.i).getAddress(),
    clientKernel
  );

  // here the client sends the previous secret to the server

  // server then signs settlement txs and refund tx
  newProviderTxs =
    server.createSettlementTransaction(
      utxosMultisig,
      state.i,
      state.n,
      newClientTxs.refundTx.tx,
      newClientTxs.revPubkey.revHash.toString(),
      serverKernel.multisig.getProviderNode(state.i).getAddress(),
      providerBalance,
      clientBalance,
      feePerByte,
      serverKernel,
    );
  // console.dir(newProviderTxs);


  t.pass();
});

test.serial(t => t.pass());

test.todo(
  'at n+1, client should be able to broadcast spendrefund tx \
after blocksInFuture'
);
test.todo(
  'client should be able to broadcast spendrefund tx after blocksInFuture'
);
/*
test.serial(
  'client should be able to broadcast spendrefund tx after blocksInFuture',
  async (t) => {
    try {
      const clientAddress = clientKernel
        .deriveFundingNode(randomInt()).getAddress();
      // console.log('normal refund',
      //     newRefundtxProviderClient.build().virtualSize()
      // );
      await adapter.broadcastTx(newRefundtxProviderClient.build().toHex());
      const utxos =
        await adapter.getUtxos(newClientTxs.revPubkey.address.toString());
      const utxosAndWifs = client.createUtxosAndWifs(
        utxos,
        clientKernel.revPubkey.getKeyPair(state.i).toWIF()
      );
      const spendRefund =
        await client.createSpendRefundTransaction(
          utxosAndWifs,
          state.i,
          state.n,
          feePerByte,
          clientAddress,
          clientKernel,
        );
      await rpc.generate(blocksInFuture);
      await adapter.broadcastTx(spendRefund.tx);
      t.pass();
    } catch (err) {
      console.error(err);
      console.log(err);
      t.fail();
    }
  });
*/

test.todo(
  'at n+1, client should not be able to broadcast spendrefund tx \
before blocksInFuture'
);

test.todo(
  'at n+1, provider should be able to broadcast settlement tx \
immediately'
);

test.todo(
  'at n+1, provider can revoke old refundtx before spendrefundtx \
becomes valid'
);
