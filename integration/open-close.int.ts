import {
  randomInt,
} from './helper';
import {
  client,
  clientKernel,
  server,
  serverKernel,
  adapter,
  rpc,
  blocksInFuture,
  feePerByte,
  TransactionBuilder,
} from './environment';
import { test } from 'ava';
import {
  UtxoAndWif,
  CreateFundingTransaction,
  Tx,
} from '../lib/common/types';
import { newI, newN, newState, newT } from '../lib/state/state';

const state = newState(
  newI(randomInt(), Infinity),
  newN(randomInt(), Infinity),
  newT(randomInt(), 0));

let utxosAndWifs: UtxoAndWif[];
test.before('client controlled address should have funds', async (t) => {
  try {
    const fundingAddressIndex = randomInt();
    const clientFundingNode =
      clientKernel.deriveFundingNode(fundingAddressIndex);
    // send funds to a client address
    const amount = 0.1;
    const cAddress = clientFundingNode.getAddress();
    await rpc.sendToAddress(cAddress, amount);
    await rpc.importAddress(cAddress);
    const utxos = await adapter.getUtxos(cAddress);
    utxosAndWifs = client
      .createUtxosAndWifs(utxos, clientFundingNode.keyPair.toWIF());
    t.is(utxos[0].amount.bitcoin(), amount);
  } catch (err) { console.error(err); t.fail(); }
});

let f: CreateFundingTransaction;
let refundtxProvider: string;
let refundtxProviderClient: TransactionBuilder;

test.serial(
  'fail to verify provider signature if he signs with wrong key',
  async (t) => {
    // create unsigned txs
    f =
      client.createFundingTransaction(
        utxosAndWifs,
        state.i,
        state.n,
        feePerByte,
        clientKernel.multisig.getProviderNode(state.i).getAddress(),
        clientKernel,
      );
    try {
      // ask provider to sign refund, he's malicious and signs with wrong key
      refundtxProvider =
        server.signRefundTransaction(
          f.refundTx.tx,
          state.i.next(), // should be i
          serverKernel,
        ).buildIncomplete().toHex();

      const { verifiedScript, verifiedSignature } =
        clientKernel.multisig.verifySignature(
          client.builderFromTransactionHex(refundtxProvider, clientKernel),
          state.i,
          'server',
        );

      t.is(verifiedScript && verifiedSignature, false);
    } catch (err) { t.pass(); }
  });

test.serial(
  'broadcast refund before the funding transaction should throw',
  async (t) => {
    // ask provider to sign refund, overwrites
    refundtxProvider =
      server.signRefundTransaction(
        f.refundTx.tx,
        state.i,
        serverKernel,
      ).buildIncomplete().toHex();
    // check provider signature

    let v = clientKernel.multisig.verifySignature(
      client.builderFromTransactionHex(refundtxProvider, clientKernel),
      state.i,
      'server',
    );

    t.is(v.verifiedScript && v.verifiedSignature, true);

    // client now signs refund
    refundtxProviderClient =
      client.cosignRefundTransaction(refundtxProvider, state.i, clientKernel);

    // check client signature
    v = serverKernel.multisig.verifySignature(
      server.builderFromTransactionHex(refundtxProviderClient, serverKernel),
      state.i,
      'client',
    );

    t.is(v.verifiedScript && v.verifiedSignature, true);

    try {
      await adapter.broadcastTx(refundtxProviderClient.build().toHex());
      t.fail();
    } catch (err) { t.pass(); }
  }
);

test.serial(
  'should broadcast funding tx correctly',
  async (t) => {
    // now broadcast fundingTx
    try {
      await adapter.broadcastTx(f.fundingTx.tx);
      t.pass();
    } catch (err) { console.error(err); t.fail(); }
  });

test.serial(
  'broadcasting partially signed refundtx should throw',
  async (t) => {
    try {
      await adapter.broadcastTx(refundtxProvider);
      t.fail();
    } catch (err) { t.pass(); }
  });

test.serial('should broadcast refund tx correctly', async (t) => {
  try {
    // broadcast refundTx
    // console.log('first refund vsize',
    //   refundtxProviderClient.build().virtualSize());
    await adapter.broadcastTx(refundtxProviderClient.build().toHex());
    t.pass();
  } catch (err) { t.fail(err); }
});

let clientAddress: string;
let missingBlocks: number;
let spendRefund: Tx;

test.serial(
  'client should fail to broadcast spendrefund tx before blocksInFuture',
  async (t) => {
    await rpc.importAddress(f.revPubkey.address.toString());
    missingBlocks = 1;
    await rpc.generate(blocksInFuture - missingBlocks);
    const utxos =
      await adapter.getUtxos(f.revPubkey.address.toString());
    clientAddress = clientKernel.multisig.getClientNode(state.i).getAddress();
    utxosAndWifs = client.createUtxosAndWifs(
      utxos,
      clientKernel.revPubkey.getKeyPair(state.i).toWIF()
    );
    spendRefund = client.createSpendRefundTransaction(
      utxosAndWifs,
      state.i,
      state.n,
      feePerByte,
      clientAddress,
      clientKernel,
    );

    try {
      await adapter.broadcastTx(spendRefund.tx);
      t.fail();
    } catch (err) { t.pass(); }
  });

test.serial(
  'client should be able to broadcast spendrefund tx after blocksInFuture',
  async (t) => {
    try {
      await rpc.generate(missingBlocks);
      await adapter.broadcastTx(spendRefund.tx);
      t.pass();
    } catch (err) { console.error(err); t.fail(); }
  });

test.serial(
  'client should be able to claim the utxo after blockInFuture expired',
  async (t) => {
    try {
      await rpc.importAddress(clientAddress);
      const utxos = await adapter.getUtxos(clientAddress);
      t.is(utxos.length > 0, true);
    } catch (err) { console.error(err); t.fail(); }
  });
