
import {
  InitiateTransaction,
  CreateFundingTransaction,
  CreateSettlementTransaction,
  TransactionBuilder,
} from './environment';
import { Btc, Utxo, UtxoAndWif } from '../lib/common/types';
import { State, newI, newN, newState, newT } from '../lib/state/state';

export const randomInt = (min = 0, max = 1e6) =>
  Math.floor(Math.random() * (max - min)) + min;

/**
 * derives the address from the kernel and the fundingAddressIndex,
 * then funds the address a certain amount
 * @param {Protocol} protocol object to create utxos and wifs
 * @param {Kernel} kernel used to derive address
 * @param {Rpc}
 * @param {Adapter}
 * @param {Number} index of the address to derive
 * @param {Number} amount to fund
 * @return {UtxoAndWif[]}
 */
const fundAddress = async (
  client,
  clientKernel,
  rpc,
  adapter,
  fundingAddressIndex,
  amount,
): Promise<UtxoAndWif[]> => {
  const clientFundingNode =
    clientKernel.deriveFundingNode(fundingAddressIndex);
  // send funds to a client address

  const cAddress = clientFundingNode.getAddress();
  await rpc.sendToAddress(cAddress, amount);
  await rpc.importAddress(cAddress);
  const utxos = await adapter.getUtxos(cAddress);
  const utxosAndWifs = client
    .createUtxosAndWifs(utxos, clientFundingNode.keyPair.toWIF());

  return utxosAndWifs;
};


/**
  * Funds a client controlled address,
  * Creates a refund transaction and a settlement transaction
  */
export const createInitialTransactions = async (
  t,
  client,
  clientKernel,
  server,
  serverKernel,
  rpc,
  adapter,
  feePerByte
): Promise<Array<any>> => {

  let state: State;
  let utxosMultisig: Utxo[];
  let multisigAddress: string;
  let f: CreateFundingTransaction;
  let newClientTxs: InitiateTransaction;
  let newProviderTxs: CreateSettlementTransaction;
  let newRefundtxProviderClient: TransactionBuilder;
  let refundtxProvider: string;

  // rpc setup
  state = newState(
    newI(randomInt(), Infinity),
    newN(randomInt(), Infinity),
    newT(0, 0)
  );

  const fundingAddressIndex = 0;
  const amount = 0.1;
  const utxosAndWifs = await fundAddress(
    client,
    clientKernel,
    rpc,
    adapter,
    fundingAddressIndex,
    amount,
  );

  // funding transaction
  f =
    client.createFundingTransaction(
      utxosAndWifs,
      state.i,
      state.n,
      feePerByte,
      clientKernel.multisig.getProviderNode(state.i).getAddress(),
      clientKernel,
    );

  // ask provider to sign refund
  refundtxProvider =
    server.signRefundTransaction(
      f.refundTx.tx,
      state.i,
      serverKernel,
    ).buildIncomplete().toHex();

  // check the signature
  const { verifiedScript, verifiedSignature } =
    clientKernel.multisig.verifySignature(
      client.builderFromTransactionHex(refundtxProvider, clientKernel),
      state.i,
      'server',
    );

  t.is(verifiedScript, true);
  t.is(verifiedSignature, true);

  // client now broadcasts funding tx
  await adapter.broadcastTx(f.fundingTx.tx);

  // negotiate first on channel transaction of the channel
  multisigAddress = f.multisig.address;

  // fetch new utxos on the funding address
  await rpc.importAddress(multisigAddress);

  utxosMultisig = await adapter.getUtxos(multisigAddress);

  const total =
    utxosMultisig.reduce((acc, utxo) => utxo.amount, Btc.zero());

  const providerBalance = Btc.fromBitcoin(0.003);
  const clientBalance = total.sub(providerBalance);

  state = state.next.n();

  newClientTxs =
    client.initiateTransaction(
      utxosMultisig,
      state.i,
      state.n,
      providerBalance,
      clientBalance,
      feePerByte,
      clientKernel.multisig.getProviderNode(state.i).getAddress(),
      clientKernel
    );

  // server signs settlement/post settlement txs and refund tx
  newProviderTxs =
    server.createSettlementTransaction(
      utxosMultisig,
      state.i,
      state.n,
      newClientTxs.refundTx.tx,
      newClientTxs.revPubkey.revHash.toString(),
      serverKernel.multisig.getProviderNode(state.i).getAddress(),
      providerBalance,
      clientBalance,
      feePerByte,
      serverKernel,
    );

  // client receives refundTx and cosigns it
  newRefundtxProviderClient =
    client.cosignRefundTransaction(
      newProviderTxs.refundTxHex,
      state.i,
      clientKernel
    );

  await rpc.importAddress(newClientTxs.revPubkey.address.toString());
  return [
    state,
    utxosMultisig,
    multisigAddress,
    f,
    newClientTxs,
    newProviderTxs,
    newRefundtxProviderClient,
    refundtxProvider,
    clientBalance,
    providerBalance,
  ];
};
