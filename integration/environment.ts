import * as RpcClient from 'bitcoin-core';
import { Server, Client } from '../lib/index';
import {
  Btc,
  CreateFundingTransaction,
  InitiateTransaction,
  CreateSettlementTransaction,
  TransactionBuilder,
} from '../lib/common/types';

import { Kernel } from '../lib/kernel/Kernel';

export {
  CreateFundingTransaction,
  InitiateTransaction,
  CreateSettlementTransaction,
  TransactionBuilder,
};

const accountId = 4;
const api = 'btcrpc';
export const blocksInFuture = 10;
const networkString = 'regtest';
const network = Client.networkFromString(networkString);
const clientAccKeys = Client.createAccKeys(accountId, network);
const providerAccKeys = Server.createAccKeys(accountId, network);

export const client =
  new Client(
  );

export const clientKernel =
  new Kernel(
    clientAccKeys.privKey,
    providerAccKeys.pubAcc,
    accountId,
    networkString,
    api,
    blocksInFuture,
  );

export const server =
  new Server(
      );

export const serverKernel =
  new Kernel(
    clientAccKeys.pubAcc,
    providerAccKeys.privKey,
    accountId,
    networkString,
    api,
    blocksInFuture,
  );


export const feePerByte = Btc.fromSatoshis(100);

export const adapter = clientKernel.getAdapter();

const host = process.env.RPC_HOST || '127.0.0.1';

export const rpc =
  new RpcClient({
    host,
    port: 18332,
    username: 'test',
    password: 'test',
  });
