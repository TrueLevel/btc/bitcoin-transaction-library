import {
  client,
  clientKernel,
  server,
  serverKernel,
  adapter,
  rpc,
  blocksInFuture,
  feePerByte,
  InitiateTransaction,
  CreateFundingTransaction,
  CreateSettlementTransaction,
  TransactionBuilder,
} from './environment';
import { test } from 'ava';
import { Utxo } from '../lib/common/types';
import { randomInt, createInitialTransactions } from './helper';

import { State } from '../lib/state/state';

let state: State;
let utxosMultisig: Utxo[];
let multisigAddress: string;
let f: CreateFundingTransaction;
let newClientTxs: InitiateTransaction;
let newProviderTxs: CreateSettlementTransaction;
let newRefundtxProviderClient: TransactionBuilder;
let refundtxProvider: string;

test.beforeEach('fund client controlled address', async (t) => {
  [
    state,
    utxosMultisig,
    multisigAddress,
    f,
    newClientTxs,
    newProviderTxs,
    newRefundtxProviderClient,
    refundtxProvider
  ] = await createInitialTransactions(
    t, // we pass t because some tests are performed in this function
    client,
    clientKernel,
    server,
    serverKernel,
    rpc,
    adapter,
    feePerByte
  );
  t.pass();
});

test.serial(
  'client should be able to broadcast spendrefund tx after blocksInFuture',
  async (t) => {
    try {
      const clientAddress = clientKernel
        .deriveFundingNode(randomInt()).getAddress();
      // console.log('normal refund',
      //     newRefundtxProviderClient.build().virtualSize()
      // );
      await adapter.broadcastTx(newRefundtxProviderClient.build().toHex());
      const utxos =
        await adapter.getUtxos(newClientTxs.revPubkey.address.toString());
      const utxosAndWifs = client.createUtxosAndWifs(
        utxos,
        clientKernel.revPubkey.getKeyPair(state.i).toWIF()
      );
      const spendRefund =
        await client.createSpendRefundTransaction(
          utxosAndWifs,
          state.i,
          state.n,
          feePerByte,
          clientAddress,
          clientKernel,
        );
      await rpc.generate(blocksInFuture);
      await adapter.broadcastTx(spendRefund.tx);
      t.pass();
    } catch (err) { console.error(err); t.fail(); }
  });


test.serial(
  'client should not be able to broadcast spendrefund tx before blocksInFuture',
  async (t) => {
    try {
      const clientAddress = clientKernel
        .deriveFundingNode(randomInt()).getAddress();
      await adapter.broadcastTx(newRefundtxProviderClient.build().toHex());
      const utxos =
        await adapter.getUtxos(newClientTxs.revPubkey.address.toString());
      const utxosAndWifs = client.createUtxosAndWifs(
        utxos,
        clientKernel.revPubkey.getKeyPair(state.i).toWIF()
      );
      const spendRefund = client.createSpendRefundTransaction(
        utxosAndWifs,
        state.i,
        state.n,
        feePerByte,
        clientAddress,
        clientKernel,
      );
      await rpc.generate(blocksInFuture - 1);
      await adapter.broadcastTx(spendRefund.tx);
      t.fail();
    } catch (err) { t.pass(); }
  }
);

test.serial(
  'provider should be able to broadcast settlement tx immediately',
  async (t) => {
    // ask client to sign the settlementTx
    const settlementtxClient =
      client.signSettlementTransaction(
        state.i,
        state.n,
        newProviderTxs.settlementTxHex,
        clientKernel,
      );

    // provider then signs
    const settlementtxClientProvider = server
      .cosignSettlementTransaction(
        settlementtxClient.settlementTxHex,
        state.i,
        serverKernel,
      );

    // console.log('settlemnt tx',
    //   await rpc.decodeRawTransaction(settlementtxClientProvider)
    // );

    await adapter.broadcastTx(settlementtxClientProvider);
    await rpc.generate(1);
    t.pass();
  }
);

test.serial(
  'provider can revoke old refundtx before spendrefundtx becomes valid',
  async (t) => {
    const refundtxProviderClient = client
      .cosignRefundTransaction(
        refundtxProvider,
        state.i,
        clientKernel,
      );
    await adapter.broadcastTx(refundtxProviderClient.build().toHex());
    await rpc.importAddress(f.revPubkey.address.toString());
    await rpc.generate(blocksInFuture - 1);
    const utxos =
      await adapter.getUtxos(f.revPubkey.address.toString());
    const utxosAndWifs = server.createUtxosAndWifs(
      utxos,
      serverKernel.revPubkey.getKeyPair(state.i).toWIF()
    );
    const revokeRefund =
      server.createRevokeRefund(
        utxosAndWifs,
        state.i,
        feePerByte,
        serverKernel.revPubkey.getProviderNode(state.i).getAddress(),
        // this secret is not known by provider before we go to n+1
        newClientTxs.previousSecret,
        serverKernel,
      );

    try {
      await adapter.broadcastTx(revokeRefund.tx);
      rpc.generate(1);
      t.pass();
    } catch (err) { console.error(err); t.fail(); }
  }
);
