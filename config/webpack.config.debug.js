var path = require('path');
var webpack = require('webpack');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;

module.exports = {
  context: path.join(__dirname, '..'),
  entry: './lib/index.ts',
  output: {
    path: path.join(__dirname, '../dist'),
    filename: 'channel.js',
    // library: 'channel',
    libraryTarget: 'commonjs'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts-loader'
      }
    ]
  },
  plugins: [
    new BundleAnalyzerPlugin(),
    // ... other plugins
  ],
  devtool: 'source-map',
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  }
};
