#!/bin/bash

if [ ! -f ./.integration-tests ]; then
    echo ".integration-tests file do not exists, try to run ./run-integration-tests.sh first!"
else
    echo "Deleting files..."
    rm .integration-tests
    echo "Deleting ~/.bitcoin/regtest folder..."
    rm -rf ~/.bitcoin/regtest/
    echo "Restarting bitcoind daemon..."
    pkill bitcoind
    sleep 3
    bitcoind -daemon
    echo "Process terminated with success!"
fi

